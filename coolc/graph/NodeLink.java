package cool.compiler.graph;

public class NodeLink{  
	  private String padre;  
	  private String hijo;
	  
	  
		public NodeLink(String padre, String hijo) {
		super();
		this.padre = padre;
		this.hijo = hijo;
	}
		/**
		 * @return the padre
		 */
		public String getPadre() {
			return padre;
		}
		/**
		 * @param padre the padre to set
		 */
		public void setPadre(String padre) {
			this.padre = padre;
		}
		/**
		 * @return the hijo
		 */
		public String getHijo() {
			return hijo;
		}
		/**
		 * @param hijo the hijo to set
		 */
		public void setHijo(String hijo) {
			this.hijo = hijo;
		}  
		   
	
		   
	} 