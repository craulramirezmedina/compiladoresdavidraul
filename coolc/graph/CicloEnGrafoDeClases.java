package cool.compiler.graph;

import java.util.*;  

public class CicloEnGrafoDeClases {  
  public CicloEnGrafoDeClases() {  
  }  
    
  public void initializeVisitTable(Hashtable visitTable){  
    visitTable.put("A", new Integer(0));  
    visitTable.put("B", new Integer(0));  
    visitTable.put("C", new Integer(0));  
    visitTable.put("D", new Integer(0));  
}  
   
  
  public void printPartialGraphs(String partialGraphs[]){  
    for(int i=0; i<partialGraphs.length; i++){  
      System.out.println(i + " partial graph " + partialGraphs[i]);  
     }  
   
  }  
   
  public boolean hayCiclo(String[] pathList, Hashtable visitTable){  
    int counter=0;  
    String temp ="";  
    String vertex = "";  
    for(int i=0; i<pathList.length; i++){  
      this.initializeVisitTable(visitTable);  
      temp = pathList[i];  
      System.out.println("Path List " + temp);  
      temp = temp.replaceAll("-", " ");  
      temp = temp.replaceAll(">", " ");  
      System.out.println("Path List " + temp);  
      for(int j=0;j<visitTable.size(); j++){  
        for(int k=0;k<temp.length(); k++){  
          if(visitTable.get(temp.substring(k, k+1))!= null){  
            Integer cnt = (Integer)visitTable.get(temp.substring(k, k+1));  
            counter = cnt.intValue() + 1;  
            visitTable.put(temp.substring(k, k+1), new Integer(counter));  
          }  
        }  
      }  
        
      //Determine if there is a cycle in this partial subgraph  
      Collection c = visitTable.values();  
      Iterator iter = c.iterator();  
      while(iter.hasNext()){  
        Integer intgr =(Integer)iter.next();  
        if(intgr.intValue()>1)  
          return true;  
      }  
        
    }  
    return false; //Completed traversing the  
  }  
   
  public void constructPaths(ArrayList al, ArrayList path, String[] partialGraphs){  
      
    for(int i=0; i<al.size(); i++){  
      NodeLink nli = (NodeLink)al.get(i);  
       //Making a partial graph for each entry in the node links  
      String from = nli.getPadre();  
      String to= nli.getHijo();  
      //path.add(from);  
      from = from + "--->" + to;  
       for(int j=i+1; j<al.size(); j++){  
        NodeLink nlj = (NodeLink)al.get(j);  
        String tempFrom = "";  
        String tempTo = "";  
        tempFrom = nlj.getPadre();  
        tempTo = nlj.getHijo();  
   
        if(to.equals(tempFrom)){  
          from = from + "--->" + tempTo;  
          to = tempTo;  
        }  
      }  
      partialGraphs[i] = from; //Making a list of partial graphs  
    }  
    for(int i=0; i<partialGraphs.length; i++){  
          System.out.println(i + " partial graph " + partialGraphs[i]);  
         }  
   
  }  
 }  
   
 