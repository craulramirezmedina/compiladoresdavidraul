class Main { 

	main() : Object {  
		(new B).shagy()
	}; 
};

class A {
	moo:Object;
		boo():Object {
			{ case moo of i:Int => i;
				b:Bool => b;
				s:String => s;
				esac; 
			}
		};		
                scoobydoo():Object {
			{ case moo of i:Int => i;
				b:Bool => b;
				s:String => s;
				esac; 
			}
		};
                shagy():Object {
			{ case moo of i:Int => i;
				b:Bool => b;
				s:String => s;
				esac; 
			}
		};
};

class B {
	moo:Int;
		boo():Object {
			{
			moo <- 4;
			moo + 5;
			}
		};
 		shagy():Object {
			{ case moo of i:Int => i;
				b:Bool => b;
				s:String => s;
				esac; 
			}
		};		

};
