class Main { 

	main() : Object {  
		(let b:Main <- new Main, c:Main <- new Main, d:Main <- new Main
			in 
			{
				b;
				c;
				d;
			}
		 
		)
	}; 
};

