	
    class C inherits B {
     
      doo:Int;
      methodC () : Int { 6 };
     
    };

    class B inherits A {
     
      foo:Int;
      methodB () : Int { 6 };
     
    };
     
    class A {
     
       bar:Int;
       methodA () : Int { 6 };
       
    };
    class Main { main () : Int { 6 }; };


