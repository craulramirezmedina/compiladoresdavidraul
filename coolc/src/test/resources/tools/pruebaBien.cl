class Main inherits IO {
  n : B <- new B;
  r : Int <- 3;
  d : Int;
  main() : Object { 
	  {
	        out_string(" Primero de A: ");
			out_string(n.metodoA());
			out_string(" Ahora de B: ");
			n.metodoB();
			out_string(" !!!! ");
			out_string(" Ahora hago una suma ");
			d <- r + r;
			out_int(r);
			out_string(" + ");
			out_int(r);
			out_string(" = ");
			out_int(d);
		
			
	  }
  };
};

class A inherits IO{
	variableA : String <- "variable de A";
	metodoA() : String {
		variableA
	};
};

class B inherits A{

	metodoB() : Object {
		out_string(" - Estoy imprimiendo desde objeto B -")
	};
	

};

