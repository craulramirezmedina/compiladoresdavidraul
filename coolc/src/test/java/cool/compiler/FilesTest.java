package cool.compiler;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import au.com.bytecode.opencsv.CSVReader;
import cool.compiler.autogen.lexer.LexerException;
import cool.compiler.autogen.parser.ParserException;
import cool.compiler.ref.SemanticException;
import cool.compiler.tables.ClassTable;

public class FilesTest {
	
	public Iterator<Object[]> readCases(String file) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(
				"src/test/resources/" + file), ';');
		ArrayList<Object[]> list = new ArrayList<Object[]>();

		String[] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			list.add( new Object[] { nextLine[0], nextLine[2] } );
		}
		
		reader.close();
		return list.iterator();
	}

	@DataProvider(name = "filesProviderOk")
	public Iterator<Object[]> readCasesOk() throws IOException {
		return readCases("cases.ok");
	}

	@DataProvider(name = "filesProviderBad")
	public Iterator<Object[]> readCasesBad() throws IOException {
		return readCases("cases.bad");
	}

	
	@Test(dataProvider = "filesProviderOk")
	public void testOk(String file, String testName) throws SemanticException, IOException, LexerException, ParserException, cool.compiler.ref.SemanticException, cool.compiler.SemanticException {
	//	Errors.getInstance().reset();
	//	ClassTable.resetInstance();
		
		PrintStream out = new PrintStream(new FileOutputStream("src/test/resources/output/" + file + ".out"));
		Main m = new Main();
		m.semanticCheck(m.parseCheck("src/test/resources/input/" + file, out), out);
		
		assert true;
		
//		Iterator<String> refLines = FileUtils.readLines(new File("src/test/resources/reference/" + file + ".out")).iterator();
//		Iterator<String> outLines = FileUtils.readLines(new File("src/test/resources/output/" + file + ".out")).iterator();
//		
//		
//		while(true) {				
//			if (!refLines.hasNext()) break;
//			String r = refLines.next();
//			String o = outLines.next();
//			assert r.compareTo(o) == 0 : String.format("%s -> %s, reference=[%s], output=[%s]", file, testName, r, o);
//		}
		
	}

	@Test(dataProvider = "filesProviderBad", expectedExceptions = {SemanticException.class})
	public void testBad(String file, String testName) throws LexerException, IOException, ParserException, SemanticException, SemanticException, cool.compiler.SemanticException {
		//Errors.getInstance().reset();
		PrintStream out = new PrintStream(new FileOutputStream("src/test/resources/output/" + file + ".out"));
		Main m = new Main();
		m.semanticCheck(m.parseCheck("src/test/resources/input/" + file, out), out);
		
		assert true;
		
	}

}
