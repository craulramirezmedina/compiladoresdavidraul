package cool.compiler;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PushbackReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import cool.compiler.autogen.lexer.LexerException;
import cool.compiler.autogen.node.AClassDecl;
import cool.compiler.autogen.node.AMethodFeature;
import cool.compiler.autogen.node.Start;
import cool.compiler.autogen.parser.Parser;
import cool.compiler.autogen.parser.ParserException;
import cool.compiler.ref.tables.AbstractTable;
import cool.compiler.ref.tables.ClassTable;
import cool.compiler.symbols.AbstractSymbol;
import cool.compiler.symbols.SymbolTable;
import cool.compiler.visitors.ASTPrinter;
import cool.compiler.visitors.ASTPrinterTypes;
import cool.compiler.visitors.CodeGenInitClassMethodsAttributesVisitor;
import cool.compiler.visitors.CodeGenInitVisitor;
import cool.compiler.visitors.CodeGenVisitor;


public class Main {
	public static String file = "src/test/resources/tools/pruebaBien.cl";
	public static String fileName;
	
	private CoolLexer lexer;
	private Parser parser;
	
	public CoolLexer getLexer() {
		return lexer;
	}
	
	public static void main(String [] args) throws LexerException, IOException, ParserException, cool.compiler.ref.SemanticException {
		Main m = new Main();
		Start start = null;
		String[] arr = file.split("/");
		fileName = arr[arr.length-1];
		
		if (args.length > 0) {
			file = args[0];
			
		}
		
		try {
			start = m.parseCheck(file, System.err);
		} catch (ParserException e) {
			System.err.format("\"%s\", Syntax error at or near [%s]\nLast good token was [%s]\n%s\n",
					file, e.getToken().getText(), m.getLexer().getLastToken().getText(),
					e.getMessage());
			System.err.format("Compilation halted due to parse errors.\n");
			
			System.exit(-1);
		}

		try {
			m.semanticCheck(start, System.err);
		} catch (SemanticException e) {
			System.err.format("La compilación se ha detenido por errores semánticos\n");			
			System.exit(-1);
		}
		

	}
	
	public Start parseCheck(String file, PrintStream out) throws LexerException, IOException, ParserException {
		lexer = new CoolLexer(new PushbackReader(new FileReader(file)), out);
		parser = new Parser(lexer);
		lexer.setParser(parser);
		
		Start start = parser.parse();
		
		return start;
	}

	public void semanticCheck(Start start, PrintStream out) throws SemanticException, cool.compiler.ref.SemanticException {

		//Comenzamos con el visitor de jerarquía
		//Tendremos un Hashtable donde guardaremos un registro de sus clases y de las que heredan
		// y sobre esta, haremos los chequeos de ciclos.
		/*
		Hashtable<String, Stack<String>> registroHeredan = new Hashtable<>();
		HierarchyCheckPass hcp = new HierarchyCheckPass(registroHeredan, out);
		start.apply(hcp);		
		if (hcp.hasErrors()){
			throw new SemanticException();
		}

		Hashtable<String, Map> classesTable = new Hashtable();
		Hashtable<String, Map> classesMethodsTable = new Hashtable();
		IdentificationPass ip = new IdentificationPass(classesTable, classesMethodsTable, out);
		start.apply(ip);
		
		if (ip.hasErrors()){
			throw new SemanticException();
		}
		
		IdentificationHierarchyPass ihp = new IdentificationHierarchyPass(registroHeredan, classesTable, out);
		start.apply(ihp);
		
		if (ihp.hasErrors()){
			throw new SemanticException();
		} 
		
		TypeCheckPass tcp = new TypeCheckPass(classesMethodsTable, out);
		start.apply(tcp);
			
		if (tcp.hasErrors()){
				throw new SemanticException();
		} 
			
		start.apply(new ASTPrinterTypes(out));
	*/	
		SemanticFacade.semanticCheck(start, out);
		Map<cool.compiler.ref.symbols.AbstractSymbol, AClassDecl> map = ClassTable.getInstance().getTable();
		//start.apply(new ASTPrinterTypes(out));
	    
	    TreeMap<String, String> heredaMap = new TreeMap();
	    TreeMap<String, LinkedList<String>> className_MethodsName = new TreeMap();
	    TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta = new TreeMap();
	    TreeMap<String, TreeMap<String, String>> className_AttributesName = new TreeMap();
	    
		CodeGenInitVisitor cig = new CodeGenInitVisitor(heredaMap, className_MethodsName, className_AttributesName, className_MethodsNameCompleta);
		
		start.apply(cig);
		
        CodeGenInitClassMethodsAttributesVisitor cigcm = new CodeGenInitClassMethodsAttributesVisitor(heredaMap, className_MethodsName, className_AttributesName, className_MethodsNameCompleta);
		
		start.apply(cigcm);

		CodeGenVisitor cg = new CodeGenVisitor(map, start, out, fileName, heredaMap, className_MethodsNameCompleta, className_AttributesName);
		start.apply(cg);
		
	}

}
