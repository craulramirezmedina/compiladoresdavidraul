package cool.compiler.visitors;

import java.io.PrintStream;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;


import cool.compiler.autogen.analysis.DepthFirstAdapter;
import cool.compiler.autogen.node.AAssignExpr;
import cool.compiler.autogen.node.AAtExpr;
import cool.compiler.autogen.node.AAttributeFeature;
import cool.compiler.autogen.node.ABlockExpr;
import cool.compiler.autogen.node.ABoolExpr;
import cool.compiler.autogen.node.ABranch;
import cool.compiler.autogen.node.ACallExpr;
import cool.compiler.autogen.node.ACaseExpr;
import cool.compiler.autogen.node.AClassDecl;
import cool.compiler.autogen.node.ADivExpr;
import cool.compiler.autogen.node.AEqExpr;
import cool.compiler.autogen.node.AFormal;
import cool.compiler.autogen.node.AIfExpr;
import cool.compiler.autogen.node.AIntExpr;
import cool.compiler.autogen.node.AIsvoidExpr;
import cool.compiler.autogen.node.ALeExpr;
import cool.compiler.autogen.node.ALetDecl;
import cool.compiler.autogen.node.ALetExpr;
import cool.compiler.autogen.node.ALtExpr;
import cool.compiler.autogen.node.AMethodFeature;
import cool.compiler.autogen.node.AMinusExpr;
import cool.compiler.autogen.node.AMultExpr;
import cool.compiler.autogen.node.ANegExpr;
import cool.compiler.autogen.node.ANewExpr;
import cool.compiler.autogen.node.ANotExpr;
import cool.compiler.autogen.node.AObjectExpr;
import cool.compiler.autogen.node.APlusExpr;
import cool.compiler.autogen.node.AProgram;
import cool.compiler.autogen.node.AStrExpr;
import cool.compiler.autogen.node.AWhileExpr;
import cool.compiler.autogen.node.PBranch;
import cool.compiler.autogen.node.PExpr;
import cool.compiler.autogen.node.PFeature;
import cool.compiler.autogen.node.PFormal;
import cool.compiler.autogen.node.Start;
import cool.compiler.ref.symbols.AbstractSymbol;
import cool.compiler.ref.tables.AbstractTable;
import cool.compiler.symbols.*;
import cool.compiler.tables.ClassTable;
import cool.compiler.tables.SymbolTableN;

public class CodeGenVisitor extends DepthFirstAdapter{

	/*
	 * CodeGenVisitor
	 *  Genera C�digo en ensamblador.
	*/
	
    PrintStream out;
    Map<AbstractSymbol, AClassDecl> map;
    String fileName;
    Map<String, String> classStrTable;
    Map<String, String> methodsName_AssemblerCode_Table;
    TreeMap<String, String> heredaMap = new TreeMap();
    TreeMap<String, TreeMap<String,String>> className_AttributesName = new TreeMap();
    boolean imprimeSalidaMetodo = true;
    int sRegisterCounter= 0;
    TreeMap<String,String> variablesScope;
    String currentClassName;
    String currentMethodName;
    Stack<String> intConstCopy;
    Stack<String> stackStringGbl;
    TreeMap<Integer,Object[]> idTable;
    int contadorPilaVariables = 0;
    boolean plusRegister = false;
    TreeMap<String,Integer> clases_Numero_Almacenamiento;
    int VariablesGlobalStack = 0;
    
    int a0 =0;
    
    String text1_basicDeclarations = "";
    String text1_1_classNameTab = "";
    String text2_dynamic_dispTab= "";
    String text3_static_dispTab= "";
    String text4_dynamic_protObj= "";
    String text4_1_Main_protObj= "";
    String text5_static_protObj= "";
    String text6_heap_start= "";
    String text7_obj_init= "";
    String text8_dynamic_init= "";
    String text9_static_init= "";
    String text10_methods= "";
    String textMain_main="";
    String textlabels="";
    int numeroLabel = 0;
    int contadorClase=5;
    TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta;
  

    // Constructor
    public CodeGenVisitor(Map<AbstractSymbol, AClassDecl> map, Start start,
			PrintStream out, String fileName, TreeMap<String, String> heredaMap, TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta, TreeMap<String, TreeMap<String, String>> className_AttributesName) {
    	this.out = out;
    	this.map = map;
    	this.fileName = fileName;
        this.className_MethodsNameCompleta = className_MethodsNameCompleta;
        
    	 classStrTable = new TreeMap<String, String>();
    	 methodsName_AssemblerCode_Table = new TreeMap<String, String>();
    	 this.heredaMap = heredaMap;
clases_Numero_Almacenamiento = new TreeMap();
    	 this.className_AttributesName = className_AttributesName;
	}

	@Override
    public void inAProgram(AProgram m){

		text1_1_classNameTab = "\t .data \n";
		text1_1_classNameTab = text1_1_classNameTab.concat("\t .align 2 \n");
		text1_1_classNameTab = text1_1_classNameTab.concat("\t .globl  class_nameTab \n");

		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  bool_const0 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  bool_const1 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _int_tag \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _bool_tag \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _string_tag \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_int_tag: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   2 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_bool_tag: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   3 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_string_tag: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   4 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _MemMgr_INITIALIZER \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_MemMgr_INITIALIZER: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   _NoGC_Init \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _MemMgr_COLLECTOR \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_MemMgr_COLLECTOR: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   _NoGC_Collect \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .globl  _MemMgr_TEST \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("_MemMgr_TEST: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   0 \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   -1 \n");
		
		//LLENO MI PILA DE STRINGS CON LOS STRINGS USADOS EN EL PROGRAMA
		Stack<String> strconst = new Stack<String>();
		strconst.push(fileName);
		Enumeration<cool.compiler.ref.symbols.StringSymbol> e = AbstractTable.stringtable.getSymbols();
        while(e.hasMoreElements()){
        	String n = e.nextElement().toString();

	        strconst.push(n);

	    }
	    strconst.push("Object");
	    classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
	    strconst.push("IO");
	    classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
	    strconst.push("Int");
        classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
	    strconst.push("Bool");
	    classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
	    strconst.push("String");
	    classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
	    
	    //Los de mis clases
		Iterator itr = map.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry me = (Map.Entry)itr.next();
			
			if (!me.getKey().toString().equals("Object")&&!me.getKey().toString().equals("IO")&&!me.getKey().toString().equals("Int")&&!me.getKey().toString().equals("Bool")&&!me.getKey().toString().equals("String")&&!me.getKey().toString().equals("SELF_TYPE")){
				 strconst.push(me.getKey().toString());
				 classStrTable.put(strconst.peek(),  "str_const" + Integer.toString((strconst.size()-1)));
			}
			
		}
		if (!strconst.contains("")){
		strconst.push("");
		}
		stackStringGbl= (Stack<String>) strconst.clone();
		Stack<String>stackStringCopy  = (Stack<String>) strconst.clone();
		
		//Lleno con los intConst
		Stack<String> intconst  = new Stack<String>();
		Enumeration<cool.compiler.ref.symbols.IntSymbol> eInt = AbstractTable.inttable.getSymbols();
		
	    while(eInt.hasMoreElements()){

	    	String nxt = eInt.nextElement().toString();
	    	if(!intconst.contains(nxt)){
	    		intconst.push(nxt);
	    	}
	    	
	    }
	    
	    while (!stackStringCopy.isEmpty()){
			if (stackStringCopy.peek().equals("")){
				 // Agrego el nulo
				if(!intconst.contains("0")){
					intconst.push("0");
		    	}
			    
			}
			else {
				
				if(!intconst.contains(Integer.toString(stackStringCopy.peek().length()))){
					intconst.push(Integer.toString(stackStringCopy.peek().length()));
		    	}
			}
				stackStringCopy.pop();
			}
	   
	    intConstCopy = (Stack<String>) intconst.clone();
	    
		while (!strconst.isEmpty()){
			text1_basicDeclarations = text1_basicDeclarations.concat("str_const" + (strconst.size()-1) + ": \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   4 \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   " + strconst.peek().length() + "\n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   String_dispTab \n");
			
			if(strconst.peek().equals("")||strconst.peek().equals("")){
				text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   int_const" + Integer.toString((intConstCopy.indexOf("0") )) + "\n");
			}
			else {
			  text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   int_const" + Integer.toString((intConstCopy.indexOf( Integer.toString((strconst.peek().length()) ) ))) + "\n");
			  text1_basicDeclarations = text1_basicDeclarations.concat("\t .ascii  \"" +strconst.peek() +"\" \n");
			}
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .byte   0 \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .align  2 \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   -1 \n");
			strconst.pop();
		}
		
	    while (!intconst.isEmpty()){
	    	text1_basicDeclarations = text1_basicDeclarations.concat("int_const" + (intconst.size()-1) + ": \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   2 \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   4 \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Int_dispTab \n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   "+ intconst.peek() + "\n");
			text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   -1 \n");
			intconst.pop();
	    }
	    
	    text1_basicDeclarations = text1_basicDeclarations.concat("bool_const0: \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   3 \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   4 \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Bool_dispTab \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   0 \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   -1 \n");

	    text1_basicDeclarations = text1_basicDeclarations.concat("bool_const1: \n");
    	text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   3 \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   4 \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Bool_dispTab \n");
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   1 \n");
	    
	    text1_basicDeclarations = text1_basicDeclarations.concat("class_nameTab: \n");
	    
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   str_const"+ stackStringGbl.indexOf("Object") +"\n"); //Object
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   str_const"+ stackStringGbl.indexOf("IO") +"\n"); // IO
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   str_const"+ stackStringGbl.indexOf("Int") +"\n"); // Int
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   str_const"+ stackStringGbl.indexOf("Bool") +"\n"); // Bool
	    text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   str_const"+ stackStringGbl.indexOf("String") +"\n"); // String
	    
	    Iterator itr2 = classStrTable.entrySet().iterator();
		while (itr2.hasNext()) {
			Map.Entry me = (Map.Entry)itr2.next();
			if (!me.getKey().toString().equals("Object")&&!me.getKey().toString().equals("IO")&&!me.getKey().toString().equals("Int")&&!me.getKey().toString().equals("Bool")&&!me.getKey().toString().equals("String")&&!me.getKey().toString().equals("SELF_TYPE")){
			 text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   "+ me.getValue().toString()+ "\n");
			}
			
		}
		
		text1_basicDeclarations = text1_basicDeclarations.concat("class_objTab: \n");
	    Iterator itr3 = classStrTable.entrySet().iterator();
		while (itr3.hasNext()) {
			Map.Entry me = (Map.Entry)itr3.next();
          	 text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   "+ (me.getKey().toString().split(" "))[0]+"_protObj \n");
			 text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   "+ (me.getKey().toString().split(" "))[0]+"_init \n");
			
		}
		
	    Iterator itr4 = classStrTable.entrySet().iterator();
		while (itr4.hasNext()) {
			Map.Entry me = (Map.Entry)itr4.next();
			  text1_1_classNameTab = text1_1_classNameTab.concat("\t .globl "+  (me.getKey().toString().split(" "))[0]+"_protObj \n");
		}
		
		text1_basicDeclarations = text1_basicDeclarations.concat("Object_dispTab: \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.abort \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.type_name \n");
		text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.copy \n");


		text1_basicDeclarations = text1_basicDeclarations.concat("String_dispTab: \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.abort \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.type_name \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.copy \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   String.length \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   String.concat \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   String.substr \n");
        

        
        text1_basicDeclarations = text1_basicDeclarations.concat("Bool_dispTab: \n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.abort\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.type_name\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.copy\n");
        
	    


    	
        text1_basicDeclarations = text1_basicDeclarations.concat("Int_dispTab:\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.abort\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.type_name\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.copy\n");

        text1_basicDeclarations = text1_basicDeclarations.concat("IO_dispTab:\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.abort\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.type_name\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   Object.copy\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   IO.out_string\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   IO.out_int\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   IO.in_string\n");
        text1_basicDeclarations = text1_basicDeclarations.concat("\t .word   IO.in_int\n");
        text3_static_dispTab  = text3_static_dispTab.concat("\t .word   -1\n");
        
	   
        
        text3_static_dispTab  = text3_static_dispTab.concat("Object_protObj:\n");
        clases_Numero_Almacenamiento.put("Object", 0);
        text3_static_dispTab  = text3_static_dispTab.concat("\t .word   0\n");
        text3_static_dispTab  = text3_static_dispTab.concat("\t .word   3\n");
        text3_static_dispTab  = text3_static_dispTab.concat("\t .word   Object_dispTab\n");
        text3_static_dispTab  = text3_static_dispTab.concat("\t .word   -1\n");
        
        // MAIN_PROTOBJ

        text5_static_protObj  = text5_static_protObj.concat("String_protObj:\n");
        clases_Numero_Almacenamiento.put("String", 4);
		text5_static_protObj  = text5_static_protObj.concat("\t .word   4\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   5\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   String_dispTab\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   int_const"+Integer.toString((intConstCopy.indexOf("0"))) + "\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   0\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   -1\n");
		
		text5_static_protObj  = text5_static_protObj.concat("Bool_protObj:\n");
		clases_Numero_Almacenamiento.put("Bool", 3);
		text5_static_protObj  = text5_static_protObj.concat("\t .word   3\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   4\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   Bool_dispTab\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   0\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   -1\n");

		text5_static_protObj  = text5_static_protObj.concat("Int_protObj:\n");
		clases_Numero_Almacenamiento.put("Int", 2);
		text5_static_protObj  = text5_static_protObj.concat("\t .word   2\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   4\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   Int_dispTab\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   0\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   -1\n");

		text5_static_protObj  = text5_static_protObj.concat("IO_protObj:\n");
		clases_Numero_Almacenamiento.put("IO", 1);
		text5_static_protObj  = text5_static_protObj.concat("\t .word   1\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   3\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   IO_dispTab\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   0\n");
		text5_static_protObj  = text5_static_protObj.concat("\t .word   -1\n");

		
		
		

		text7_obj_init  = text7_obj_init.concat("Object_init:\n");
		text7_obj_init  = text7_obj_init.concat("\t addiu   $sp $sp -12\n");
		text7_obj_init  = text7_obj_init.concat("\t sw      $fp 12($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t sw      $s0 8($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t sw      $ra 4($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t addiu   $fp $sp 4\n");
		text7_obj_init  = text7_obj_init.concat("\t move    $s0 $a0\n");
		text7_obj_init  = text7_obj_init.concat("\t move    $a0 $s0\n");
		text7_obj_init  = text7_obj_init.concat("\t lw      $fp 12($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t lw      $s0 8($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t lw      $ra 4($sp)\n");
		text7_obj_init  = text7_obj_init.concat("\t addiu   $sp $sp 12\n");
		text7_obj_init  = text7_obj_init.concat("\t jr      $ra\n");
		
		// MAIN INIT
		
		text9_static_init = text9_static_init.concat("String_init:\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp -12\n");
		text9_static_init = text9_static_init.concat("\t sw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $fp $sp 4\n");
		text9_static_init = text9_static_init.concat("\t move    $s0 $a0\n");
		text9_static_init = text9_static_init.concat("\t jal     Object_init\n");
		text9_static_init = text9_static_init.concat("\t move    $a0 $s0\n");
		text9_static_init = text9_static_init.concat("\t lw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp 12\n");
		text9_static_init = text9_static_init.concat("\t jr      $ra\n");    

		text9_static_init = text9_static_init.concat("Bool_init:\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp -12\n");
		text9_static_init = text9_static_init.concat("\t sw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $fp $sp 4\n");
		text9_static_init = text9_static_init.concat("\t move    $s0 $a0\n");
		text9_static_init = text9_static_init.concat("\t jal     Object_init\n");
		text9_static_init = text9_static_init.concat("\t move    $a0 $s0\n");
		text9_static_init = text9_static_init.concat("\t lw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp 12\n");
		text9_static_init = text9_static_init.concat("\t jr      $ra\n");     

		text9_static_init = text9_static_init.concat("Int_init:\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp -12\n");
		text9_static_init = text9_static_init.concat("\t sw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $fp $sp 4\n");
		text9_static_init = text9_static_init.concat("\t move    $s0 $a0\n");
		text9_static_init = text9_static_init.concat("\t jal     Object_init\n");
		text9_static_init = text9_static_init.concat("\t move    $a0 $s0\n");
		text9_static_init = text9_static_init.concat("\t lw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp 12\n");
		text9_static_init = text9_static_init.concat("\t jr      $ra\n");     

		text9_static_init = text9_static_init.concat("IO_init:\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp -12\n");
		text9_static_init = text9_static_init.concat("\t sw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t sw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $fp $sp 4\n");
		text9_static_init = text9_static_init.concat("\t move    $s0 $a0\n");
		text9_static_init = text9_static_init.concat("\t jal     Object_init\n");
		text9_static_init = text9_static_init.concat("\t move    $a0 $s0\n");
		text9_static_init = text9_static_init.concat("\t lw      $fp 12($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $s0 8($sp)\n");
		text9_static_init = text9_static_init.concat("\t lw      $ra 4($sp)\n");
		text9_static_init = text9_static_init.concat("\t addiu   $sp $sp 12\n");
		text9_static_init = text9_static_init.concat("\t jr      $ra\n");     

		// MAIN.MAIN
	
		text6_heap_start = text6_heap_start.concat("heap_start:\n");
		text6_heap_start = text6_heap_start.concat("\t .word   0\n");
		text6_heap_start = text6_heap_start.concat("\t .text\n");
		text6_heap_start = text6_heap_start.concat("\t .globl  Main_init\n");
		text6_heap_start = text6_heap_start.concat("\t .globl  Int_init\n");
		text6_heap_start = text6_heap_start.concat("\t .globl  String_init\n");
		text6_heap_start = text6_heap_start.concat("\t .globl  Bool_init\n");
		text6_heap_start = text6_heap_start.concat("\t .globl  Main.main\n");
		///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//out.println("\t .globl  Main.main\n");
		//out.println("\t .globl  Main_init\n");

		 
		

    }
    
    @Override
    //Hay q hacer un open scope donde recidira la asociacion de la variable definida
    public void inAClassDecl(AClassDecl n){
    	idTable = new TreeMap();
    	variablesScope = new TreeMap();
    	contadorPilaVariables=0;
    	
    	currentClassName = (n.getName().toString().split(" "))[0];
    	
    	String hereda = n.getInherits().toString();

    	
    	LinkedList<PFeature> lkn = n.getFeature();
    	LinkedList<PFeature> lk = (LinkedList<PFeature>) lkn.clone();
    	Iterator iterador = lk.iterator();

    	text2_dynamic_dispTab = text2_dynamic_dispTab.concat(currentClassName + "_dispTab:" + "\n");
    	TreeMap<String, LinkedList<String>> metodos = className_MethodsNameCompleta.get(currentClassName);
		
		Iterator itrMetodos = metodos.entrySet().iterator();;
		while (itrMetodos.hasNext()) {
			Map.Entry me = (Map.Entry)itrMetodos.next();
			String nameClass = (String) me.getKey();
			LinkedList<String> nameMethods = (LinkedList<String>) ((LinkedList) me.getValue()).clone();
			while(!nameMethods.isEmpty()){
				text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   " + me.getKey() + "." + nameMethods.poll()+"\n");
			}
		  
		    
		}
		
		while(iterador.hasNext())
    	{
    	        PFeature o = lk.peek();
    	        if (o.getClass().toString().contains("AMethodFeature")){
    	        	AMethodFeature m = (AMethodFeature)lk.poll();
    	        	methodsName_AssemblerCode_Table.put(currentClassName + "." + (m.getObjectId().toString().split(" "))[0], "");
    	       // 	text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   "+ currentClassName + "." + (m.getObjectId().toString().split(" "))[0] + "\n"); 
    	        ////****
    	        //	Object[] tmp = {m.getObjectId().toString().trim(), m.getExpr()};
    	        //	idTable.put(contadorPilaVariables, tmp);
    	        ////****
    	        }
    	        else if (o.getClass().toString().contains("AttributeFeature")) {
    	        	AAttributeFeature m = (AAttributeFeature)lk.poll();
    	        	contadorPilaVariables++;
    	        	VariablesGlobalStack++;
    	        	Object[] tmp = {m.getObjectId().toString().trim(), m.getExpr(), m.getTypeId().toString() };
    	        	variablesScope.put(m.getObjectId().toString().trim(), m.getTypeId().toString().trim());
    	        	idTable.put(contadorPilaVariables, tmp);
    	        	
    	        	//System.out.println("no");
    	        }
    	        
    	        
    	}
		
		
    	
    	
    //	text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   Object.abort"+ "\n");
    //	text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   Object.type_name"+ "\n");
    //	text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   Object.copy"+ "\n");
    	/*
    	if (!heredaMap.get(currentClassName.trim()).equals("Object")){
    		//Si no hereda de Object, hay que cargarle los metodos de TODAS las clases que hereda.
    		
    		//Primero obtengo todas las clases de las que hereda
    		Stack<String> pilaHereda = new Stack<String>();
    		String clasetmp;
    		String heredatmp;
    		clasetmp = currentClassName.trim();
    		
    		while (!clasetmp.equals("Object")){
	    		heredatmp= heredaMap.get(clasetmp).trim();
	    		if(!heredatmp.equals("Object")){
	    			pilaHereda.push(heredaMap.get(clasetmp));
	    		}
	    		clasetmp = heredatmp.trim();
    		}
    		while (!pilaHereda.isEmpty()){
	    		String padre = pilaHereda.pop();
	    		LinkedList methodsPadre = (LinkedList) className_MethodsName.get(padre).clone();
	    		LinkedList methodsThisClass = (LinkedList) className_MethodsName.get(currentClassName.trim()).clone();
	    		TreeMap<String, String> atributosPadre = (TreeMap)className_AttributesName.get(padre).clone();
	    		TreeMap atributosThisClass = (TreeMap)className_AttributesName.get(currentClassName.trim()).clone();
	    		
	    		Iterator a = methodsPadre.iterator();
	        	
	        	while(a.hasNext())
	        	{
	        	        String methodName = (String) methodsPadre.poll();
	        	        text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   "+ padre.trim() + "." + methodName + "\n");   	
	        	        methodsThisClass.push(methodName);
	        	        
	        	        //  text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word DAVIDPACHECO\n");  
	
	        	 }
	        	 className_MethodsName.put(currentClassName.trim(),methodsThisClass);
	        	 
	        	 for (Map.Entry<String, String> entry : atributosPadre.entrySet())
	        	 {
	        		 String atributeName = (String) entry.getKey();
	        		 atributosThisClass.put(atributeName, entry.getValue());
	        	 }
	        	 className_AttributesName.put(currentClassName.trim(),atributosThisClass);
	        	 
//	        	 className_MethodsName.clone();
    	}
    		// Ahora tengo que cargarle los metodos de todas las clases de las que hereda
    		 
    		//
    	}
    	    
    	while(iterador.hasNext())
    	{
    	        PFeature o = lk.peek();
    	        if (o.getClass().toString().contains("AMethodFeature")){
    	        	AMethodFeature m = (AMethodFeature)lk.poll();
    	        	methodsName_AssemblerCode_Table.put(currentClassName + "." + (m.getObjectId().toString().split(" "))[0], "");
    	        	text2_dynamic_dispTab = text2_dynamic_dispTab.concat("\t .word   "+ currentClassName + "." + (m.getObjectId().toString().split(" "))[0] + "\n"); 
    	        ////****
    	        //	Object[] tmp = {m.getObjectId().toString().trim(), m.getExpr()};
    	        //	idTable.put(contadorPilaVariables, tmp);
    	        ////****
    	        }
    	        else if (o.getClass().toString().contains("AttributeFeature")) {
    	        	AAttributeFeature m = (AAttributeFeature)lk.poll();
    	        	contadorPilaVariables++;
    	        	Object[] tmp = {m.getObjectId().toString().trim(), m.getExpr(), m.getTypeId().toString() };
    	        	
    	        	idTable.put(contadorPilaVariables, tmp);
    	        	
    	        	//System.out.println("no");
    	        }
    	        
    	        
    	}
    	//text6_heap_start = text6_heap_start.concat("\t .globl  "+ currentClassName + "_init\n");
    	//p.getObjectId();
    	*/
		if(currentClassName.equals("Main")){
			text4_1_Main_protObj = text4_1_Main_protObj.concat(currentClassName+ "_protObj:" + "\n");
			text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   "+ contadorClase +"\n");
			clases_Numero_Almacenamiento.put(currentClassName.trim(), 0);
			contadorClase++;
			text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   "+ (3 + idTable.size()) + "\n"); // 3 + numero variables
			text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + currentClassName + "_dispTab"+ "\n");
			
			Iterator itrVariables = idTable.entrySet().iterator();
			while (itrVariables.hasNext()) {
				Map.Entry me = (Map.Entry)itrVariables.next();
				Object[] array = (Object[]) me.getValue();
				PExpr exp = (PExpr) array[1];
				String typeId = (String) array[2];
				typeId = typeId.trim();

				if (exp!=null){
					if(exp.getClass().toString().contains("AIntExpr")){
						AIntExpr intexpr = (AIntExpr) exp;
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "int_const"+Integer.toString((intConstCopy.indexOf("0"))) + "\n");
					}
					else if(exp.getClass().toString().contains("ABoolExpr")){
						
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "bool_const0\n");
					
					}
					else if(exp.getClass().toString().contains("AStrExpr")){
						
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "str_const"+ stackStringGbl.indexOf("") +"\n");
					
					}
				}
				else {
					if(typeId.contains("Int")){

						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "int_const"+Integer.toString((intConstCopy.indexOf("0"))) + "\n");
					}
					else if(typeId.contains("Bool")){
					
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + " bool_const0\n");
							
					}
					else if(typeId.contains("String")){
			
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "str_const"+ stackStringGbl.indexOf("") +"\n");
						
					}
					else{
						text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .word   " + "0\n");
					}
				}
				
			}
		
			text4_1_Main_protObj = text4_1_Main_protObj.concat("\t .globl	heap_start\n");
		}
		else {
			//className_AttributesName.clone();
			text4_dynamic_protObj = text4_dynamic_protObj.concat(currentClassName+ "_protObj:" + "\n");
			text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   "+ contadorClase +"\n");
			clases_Numero_Almacenamiento.put(currentClassName.trim(), 0);
			contadorClase++;
			text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   "+ (3 + idTable.size()) + "\n"); // 3 + numero variables
			text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   " + currentClassName + "_dispTab"+ "\n");
			
			TreeMap<String, String> entrada = (TreeMap<String, String>) className_AttributesName.get(currentClassName.trim());
			Iterator itrVariables = entrada.entrySet().iterator();
			
			while (itrVariables.hasNext()) {
				Map.Entry me = (Map.Entry)itrVariables.next();
			//	Object[] array = (Object[]) me.getValue();
			//	PExpr exp = (PExpr) array[1];
			//	String typeId = (String) array[2];
				
				String typeId = (String) me.getValue();
				typeId = typeId.trim();
	
					if(typeId.contains("Int")){

						text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   " + "int_const"+Integer.toString((intConstCopy.indexOf("0"))) + "\n");
					}
					else if(typeId.contains("Bool")){
					
						text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   " + " bool_const0\n");
							
					}
					else if(typeId.contains("String")){
			
						text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   " + "str_const"+ stackStringGbl.indexOf("") +"\n");
						
					}
					else{
						text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   " + "0\n");
					}
				
				
			}
		
	
			
			
			text4_dynamic_protObj = text4_dynamic_protObj.concat("\t .word   -1"+ "\n");
		}
    	text8_dynamic_init = text8_dynamic_init.concat(currentClassName+ "_init:\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t addiu   $sp $sp -12\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t sw      $fp 12($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t sw      $s0 8($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t sw      $ra 4($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t addiu   $fp $sp 4\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t move    $s0 $a0\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t jal     " + heredaMap.get(currentClassName).trim()+ "_init\n");
		
		Iterator itrVariables2 = idTable.entrySet().iterator();
		while (itrVariables2.hasNext()) {
			Map.Entry me = (Map.Entry)itrVariables2.next();
			Object[] array = (Object[]) me.getValue();
			PExpr exp = (PExpr) array[1];
			String typeId = (String) array[2];
			typeId = typeId.trim();
			 int numeroVariable = (int)me.getKey();
			 
			if (exp!=null){
				  
					if(exp.getClass().toString().contains("IntExpr")){
						AIntExpr intexpr = (AIntExpr) exp;
						text8_dynamic_init = text8_dynamic_init.concat("\t la  $a0  " + "int_const"+Integer.toString((intConstCopy.indexOf(intexpr.getIntConst().toString().trim()))) + "\n");
					
					}
					else if(exp.getClass().toString().contains("BoolExpr")){
						ABoolExpr boolexpr = (ABoolExpr) exp;
						String value = boolexpr.getBoolConst().toString().trim();
						if(value.equals("true")){
							text8_dynamic_init = text8_dynamic_init.concat("\t la      $a0 bool_const1\n");}
							else {
								text8_dynamic_init = text8_dynamic_init.concat("\t la      $a0 bool_const0\n");
							}
					}
					else if(exp.getClass().toString().contains("StrExpr")){
						AStrExpr strexpr = (AStrExpr) exp;
						String value = strexpr.getStrConst().toString().trim();
						text8_dynamic_init = text8_dynamic_init.concat("\t la       $a0  " + "str_const"+ stackStringGbl.indexOf(value) +"\n");
						
					}
					else if(exp.getClass().toString().contains("ANewExpr")){
						ANewExpr expr = (ANewExpr) exp;
						text8_dynamic_init = text8_dynamic_init.concat("\t la      $a0 "+ exp.getTypeAsString().trim() + "_protObj \n");
						text8_dynamic_init = text8_dynamic_init.concat("\t jal     Object.copy \n");
						text8_dynamic_init = text8_dynamic_init.concat("\t jal     " + exp.getTypeAsString().trim() + "_init \n");
				
					}
					
					text8_dynamic_init = text8_dynamic_init.concat("\t sw  $a0  " + (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
		}
			else {/*
				if(typeId.contains("Int")){

					text8_dynamic_init = text8_dynamic_init.concat("\t la  $a0  " + "int_const"+Integer.toString((intConstCopy.indexOf("0"))) + "\n");
				}
				else if(typeId.contains("Bool")){
				
						text8_dynamic_init = text8_dynamic_init.concat("\t la      $a0 bool_const0\n");
						
				}
				else if(typeId.contains("String")){
		
					text8_dynamic_init = text8_dynamic_init.concat("\t la       $a0  " + "str_const"+ stackStringGbl.indexOf("") +"\n");
					
				}
				text8_dynamic_init = text8_dynamic_init.concat("\t sw  $a0  " + (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
		*/
			}
			
			
		}
		
		text8_dynamic_init = text8_dynamic_init.concat("\t move    $a0 $s0\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t lw      $fp 12($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t lw      $s0 8($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t lw      $ra 4($sp)\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t addiu   $sp $sp 12\n");
		text8_dynamic_init = text8_dynamic_init.concat("\t jr      $ra\n");
    }

    public void outAClassDecl(AClassDecl n){
         n.getType();
    }
    
    @Override   
    public void inAMethodFeature(AMethodFeature node)
    {
    	currentMethodName = (node.getObjectId().toString().split(" "))[0];
    	
    	if (currentClassName.equals("Main")&&currentMethodName.trim().equals("main")){
    		textMain_main="";
    		textMain_main = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
        	textMain_main = textMain_main.concat(currentClassName + "." + currentMethodName + ":" + "\n");
        	textMain_main = textMain_main.concat("\t addiu   $sp $sp -"+ (12 + ((VariablesGlobalStack -1) * 4)) + "\n");
        	
        	textMain_main = textMain_main.concat("\t sw      $fp "+ (12 + ((VariablesGlobalStack -1) * 4)) + "($sp)" + "\n");
        	textMain_main = textMain_main.concat("\t sw      $s0 "+ ((12 + ((VariablesGlobalStack -1) * 4))-4) + "($sp)" + "\n");
        	textMain_main = textMain_main.concat("\t sw      $ra "+ ((12 + ((VariablesGlobalStack -1) * 4))-8) + "($sp)" + "\n");
        	textMain_main = textMain_main.concat("\t addiu   $fp $sp 4" + "\n");
        	textMain_main = textMain_main.concat("\t move    $s0 $a0" + "\n");
        	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, textMain_main);
        	PExpr e = node.getExpr();
        	textMain_main = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
        	textMain_main = textMain_main.concat(codeGen(e, false));
        	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, textMain_main);
    	}
    	else {
    		String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
        	assemblyCode = assemblyCode.concat(currentClassName + "." + currentMethodName + ":" + "\n");
        	assemblyCode = assemblyCode.concat("\t addiu   $sp $sp -"+ (12 + ((VariablesGlobalStack -1) * 4)) + "\n");
        	
        	assemblyCode = assemblyCode.concat("\t sw      $fp "+ (12 + ((VariablesGlobalStack -1) * 4)) + "($sp)" + "\n");
        	assemblyCode = assemblyCode.concat("\t sw      $s0 "+ ((12 + ((VariablesGlobalStack -1) * 4))-4) + "($sp)" + "\n");
        	assemblyCode = assemblyCode.concat("\t sw      $ra "+ ((12 + ((VariablesGlobalStack -1) * 4))-8) + "($sp)" + "\n");
        	assemblyCode = assemblyCode.concat("\t addiu   $fp $sp 4" + "\n");
        	assemblyCode = assemblyCode.concat("\t move    $s0 $a0" + "\n");
        	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
        	PExpr e = node.getExpr();
        	assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
        	assemblyCode = assemblyCode.concat(codeGen(e, false));
        	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
    	}
    		
    
    }
    
	public String codeGen(PExpr node, boolean isFirstOperator){
		String r ="";
		if (node.getClass().toString().contains("PlusExpr")){
			return solvePlusExpr((APlusExpr) node);
		}
		else if (node.getClass().toString().contains("MinusExpr")){
			return solveMinusExpr((AMinusExpr) node);
		}
		else if(node.getClass().toString().contains("MultExpr")){
			return solveMultExpr((AMultExpr) node);
		}
		else if  (node.getClass().toString().contains("DivExpr")){
			return solveDivExpr((ADivExpr) node);
		}
		else if (node.getClass().toString().contains("CallExpr")){
			return solveCallExpr((ACallExpr) node);
		}
		else if (node.getClass().toString().contains("AtExpr")){
			return solveAtExpr((AAtExpr) node);
		}
		else if  (node.getClass().toString().contains("AIntExpr")){
			return solveIntExpr((AIntExpr) node, isFirstOperator);
		}
		else if (node.getClass().toString().contains("ABoolExpr")){
			return solveBoolExpr((ABoolExpr) node);
		}
		else if (node.getClass().toString().contains("AStrExpr")){
			return solveStrExpr((AStrExpr) node);
		}
		else if (node.getClass().toString().contains("AObjectExpr")){
			return solveObjectExpr((AObjectExpr) node, isFirstOperator);
		}
		else if (node.getClass().toString().contains("ALtExpr")){
			return solveLtExpr((ALtExpr) node);
		}
		else if (node.getClass().toString().contains("ALeExpr")){
			return solveLeExpr((ALeExpr) node);
		}
		else if (node.getClass().toString().contains("AEqExpr")){
			return solveEqExpr((AEqExpr) node);
		}
		else if (node.getClass().toString().contains("ANotExpr")){
			return solveNotExpr((ANotExpr) node);
		}
		else if (node.getClass().toString().contains("AIsvoidExpr")){
			return solveIsVoidExpr((AIsvoidExpr) node);
		}
		else if (node.getClass().toString().contains("ANegExpr")){
			return solveNegExpr((ANegExpr) node);
		}
		else if (node.getClass().toString().contains("ANewExpr")){
			return solveNewExpr((ANewExpr) node);
		}
		else if (node.getClass().toString().contains("AIfExpr")){
			return solveIfExpr((AIfExpr) node);
		}
		else if (node.getClass().toString().contains("AWhileExpr")){
			return solveWhileExpr((AWhileExpr) node);
		}
		else if (node.getClass().toString().contains("ABlockExpr")){
			return solveBlockExpr((ABlockExpr) node);
		}
		else if (node.getClass().toString().contains("AAssignExpr")){
			return solveAssignExpr((AAssignExpr) node);
		}
		else if (node.getClass().toString().contains("ACaseExpr")){
			return solveCaseExpr((ACaseExpr) node);
		}
		else if (node.getClass().toString().contains("ALetExpr")){
			return solveLetExpr((ALetExpr) node);
		}
		return r;	
			
	}

	public String solveAssignExpr(AAssignExpr node){
		
		node.getObjectId();
	

		String value = (node.getObjectId().toString().split(" "))[0];
		String assemblyCode ="";
		if(value.equals("self")){
				
		}
		else {
			assemblyCode = assemblyCode.concat(codeGen(node.getExpr(),false));
			Iterator itr = idTable.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry me = (Map.Entry)itr.next();
				Object[] array = (Object[]) me.getValue();
				PExpr exp = (PExpr) array[1];
				String nameVariable = (String) array[0];
				if(nameVariable.equals(value)){
					//YA ENCONTRE LA VARIABLE QUE BUSCABA
					int numeroVariable = (int)me.getKey();
					assemblyCode = assemblyCode.concat("\t sw    $a0 "+ (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
					/*
					if(isFirstOperator){
						
						assemblyCode = assemblyCode.concat("\t lw    $s1 "+ (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
					}
					else{
					    assemblyCode = assemblyCode.concat("\t lw    $a0 "+ (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
					}*/
					
				}
				
			}
		//	assemblyCode = assemblyCode.concat("\t lw    $a0 "+ (contadorPilaVariables + 12) +"($s0)\n");
		}
		return assemblyCode;
		
	}

	
	public String solveNewExpr(ANewExpr node){
		String classNew = node.getTypeId().toString().trim();
	//	 String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
		    String assemblyCode="";
			assemblyCode = assemblyCode.concat("\t la      $a0 " + classNew + "_protObj" +"\n"); 
			assemblyCode = assemblyCode.concat("\t jal     Object.copy\n");
			assemblyCode = assemblyCode.concat("\t jal     "+ classNew + "_init" + "\n");		
	//		methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);		
			return assemblyCode;
		
	}

	public String solveBlockExpr(ABlockExpr node){
		String assemblyCode ="";
		sRegisterCounter = 0;
		LinkedList<PExpr> a = (LinkedList<PExpr>) node.getExpr().clone();
		Iterator iteradorB = a.iterator();

    	
    	while(iteradorB.hasNext())
    	{
    		assemblyCode = assemblyCode.concat(codeGen(a.poll(),false));
    	}
		 
         return assemblyCode;
		
	}
	
	public String solveNegExpr(ANegExpr node){		
		   String assemblyCode ="";
		   assemblyCode = assemblyCode.concat(codeGen(node.getExpr(),false));
		  // String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t jal      Object.copy\n"); 
			assemblyCode = assemblyCode.concat("\t lw      $t1 12($a0)\n");
			assemblyCode = assemblyCode.concat("\t neg     $t1 $t1\n");
			assemblyCode = assemblyCode.concat("\t sw     $t1 12($a0)\n");	
			return assemblyCode;
		//	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);		
	}
	

	public String solveNotExpr(ANotExpr node){		
		   String assemblyCode ="";
		   assemblyCode = assemblyCode.concat(codeGen(node.getExpr(),false));
	//	   String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t beqz   $a0 label"+ numeroLabel + "\n"); 
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
	//		methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
			
			assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
		/*	textlabels = textlabels.concat("\t lw      $fp 12($sp)" + "\n");
			textlabels = textlabels.concat("\t lw      $s0 8($sp)" + "\n");
			textlabels = textlabels.concat("\t lw      $ra 4($sp)" + "\n");
			textlabels = textlabels.concat("\t addiu   $sp $sp 12" + "\n");
			textlabels = textlabels.concat("\t jr      $ra" + "\n");*/
			
			numeroLabel++;
			
			return assemblyCode;
		
	}

	public String solveIfExpr(AIfExpr node){		
		   String assemblyCode ="";
		    assemblyCode = assemblyCode.concat(codeGen(node.getTest(),false));
			assemblyCode = assemblyCode.concat("\t lw   $t1 12($a0)\n"); 
			assemblyCode = assemblyCode.concat("\t beqz      $t1 label"+ numeroLabel + "\n");
			// este label es el del false
			
//			methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
			
			 assemblyCode = assemblyCode.concat(codeGen(node.getTrue(),false));
			
		//	assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t b     label" + (numeroLabel + 1)+"\n"); 
			// el del true

		   
		   
			// El label FALSE
			assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");
			assemblyCode = assemblyCode.concat(codeGen(node.getFalse(),false));
			
			numeroLabel++;
			// El label TRUE
			assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");
			/*textlabels = textlabels.concat("\t lw      $fp 12($sp)" + "\n");
			textlabels = textlabels.concat("\t lw      $s0 8($sp)" + "\n");
			textlabels = textlabels.concat("\t lw      $ra 4($sp)" + "\n");
			textlabels = textlabels.concat("\t addiu   $sp $sp 12" + "\n");
			textlabels = textlabels.concat("\t jr      $ra" + "\n");*/
			numeroLabel++;
			return assemblyCode;
		
	}

	public String solveCallExpr(ACallExpr node){		
		   String assemblyCode ="";
		   LinkedList parameters = (LinkedList) node.getExpr().clone();
		   Iterator iteradorBranch = parameters.iterator();
	    	
	    	while(iteradorBranch.hasNext())
	    	{
	    	   PExpr a = (PExpr)parameters.poll();
	    	  
	    	   assemblyCode = assemblyCode.concat(codeGen(a,false));
	    	   assemblyCode = assemblyCode.concat("\t sw      $a0 0($sp)\n");
	    	   assemblyCode = assemblyCode.concat("\t addiu   $sp $sp -4\n");
	    	}
	    	
	    	assemblyCode = assemblyCode.concat("\t move    $a0 $s0\n");
	    	assemblyCode = assemblyCode.concat("\t bne     $a0 $zero label"+ numeroLabel + "\n");
	    	assemblyCode = assemblyCode.concat("\t la      $a0 str_const0\n");
	    	assemblyCode = assemblyCode.concat("\t li      $t1 1\n");
	    	assemblyCode = assemblyCode.concat("\t jal     _dispatch_abort\n");
		   
	    	assemblyCode = assemblyCode.concat("label"+ numeroLabel + ":\n");
	    	numeroLabel++;
	    	assemblyCode = assemblyCode.concat("\t lw      $t1 8($a0)\n");
	    	TreeMap metodos = (TreeMap) className_MethodsNameCompleta.get(currentClassName.trim()).clone();
			Iterator itrMetodos = metodos.entrySet().iterator();
			int contador = 0;
			boolean busca=true;
			
			while (busca&&itrMetodos.hasNext()) {
				Map.Entry me = (Map.Entry)itrMetodos.next();
				String nameClass = (String) me.getKey();
				LinkedList<String> nameMethods = (LinkedList<String>) ((LinkedList) me.getValue()).clone();
				
				while(busca&&!nameMethods.isEmpty()){
					if(nameMethods.peek().trim().equals(node.getObjectId().toString().trim()))
						busca = false;
					else{
						contador++;
						nameMethods.poll();	
					}
				}
			  
			    
			}
			
	    	String nombreMetodoL = node.getObjectId().toString().trim();
	    	
	    	assemblyCode = assemblyCode.concat("\t lw      $t1 "+ (contador*4) + "($t1)\n");
	    	assemblyCode = assemblyCode.concat("\t jalr            $t1\n");
		   
			return assemblyCode;
		
	}
	public String getTypeIdFromUnknown(PExpr node){
		String type = "";
		if (node.getClass().toString().contains("NewExpr")){
			 ANewExpr n = (ANewExpr) node;
			 type = n.getTypeId().toString().trim();
		}
		else if (node.getClass().toString().contains("ObjectExpr")){
			 AObjectExpr n = (AObjectExpr) node;
			 type =  node.getTypeAsString();
		}
		else if (node.getClass().toString().contains("IntExpr")){
			 AIntExpr n = (AIntExpr) node;
			 type =  node.getTypeAsString();
		}
		return type;
	}
	public String solveAtExpr(AAtExpr node){		
		   String assemblyCode ="";
		   
		   assemblyCode = assemblyCode.concat(codeGen(node.getExpr(),false));
		   int labelForThis = numeroLabel;
		   numeroLabel++;
		   assemblyCode = assemblyCode.concat("\t bne $a0 $zero label"+ labelForThis + "\n");
		   assemblyCode = assemblyCode.concat("\t la  $a0 str_const0\n");
		   assemblyCode = assemblyCode.concat("\t li  $t1 1\n");
		   assemblyCode = assemblyCode.concat("\t jal _dispatch_abort\n");
		   
	    	assemblyCode = assemblyCode.concat("label"+ labelForThis + ":\n");
//	    	numeroLabel++;
	    	assemblyCode = assemblyCode.concat("\t lw      $t1 8($a0)\n");
	    	idTable.clone();
	    	//AObjectExpr oe = (AObjectExpr) node.getExpr().getType().toString();
	    	String tipo = (node.getExpr().getType().toString().split(" "))[0];

	    	TreeMap metodos = (TreeMap) className_MethodsNameCompleta.get(tipo).clone();
			Iterator itrMetodos = metodos.entrySet().iterator();
			int contador = 0;
			boolean busca=true;
			
			while (busca&&itrMetodos.hasNext()) {
				Map.Entry me = (Map.Entry)itrMetodos.next();
				String nameClass = (String) me.getKey();
				LinkedList<String> nameMethods = (LinkedList<String>) ((LinkedList) me.getValue()).clone();
				
				while(busca&&!nameMethods.isEmpty()){
					if(nameMethods.peek().trim().equals(node.getObjectId().toString().trim()))
						busca = false;
					else{
						contador++;
						nameMethods.poll();	
					}
				}
			  
			    
			}
			
	    	String nombreMetodoL = node.getObjectId().toString().trim();
	    	
	    	assemblyCode = assemblyCode.concat("\t lw      $t1 "+ (contador*4) + "($t1)\n");
	    	assemblyCode = assemblyCode.concat("\t jalr            $t1\n");
			return assemblyCode;
		
	}	
	public String solveCaseExpr(ACaseExpr node){		
		   String assemblyCode ="";
		   assemblyCode = assemblyCode.concat(codeGen(node.getTest(),false));
		   int labelForThis = numeroLabel;
		  
		   
		   numeroLabel++;
		   assemblyCode = assemblyCode.concat("\t bne    $a0 $zero label"+ numeroLabel + "\n");
		   assemblyCode = assemblyCode.concat("\t la     $a0 str_const0\n");
		   assemblyCode = assemblyCode.concat("\t li     $t1 1\n");
		   assemblyCode = assemblyCode.concat("\t jal    _case_abort2\n");
		   
		   boolean first= true;
		   
		   LinkedList<PBranch> branches = (LinkedList<PBranch>) node.getBranch().clone();
		   Iterator iteradorBranch = branches.iterator();
	    	
	    	while(iteradorBranch.hasNext())
	    	{
	    		ABranch tmp = (ABranch) branches.poll();
    	        int numeroClase = clases_Numero_Almacenamiento.get(tmp.getTypeId().toString().trim());
    	        
	 		   assemblyCode = assemblyCode.concat("label"+ numeroLabel + ":\n");
	 		   if (first){
	 			   assemblyCode = assemblyCode.concat("\t lw     $t2 0($a0)\n");
	 			   first = false;
	 		   }
			   assemblyCode = assemblyCode.concat("\t blt $t2 " + numeroClase + " label" + (numeroLabel + 1) +"\n");
			   assemblyCode = assemblyCode.concat("\t bgt $t2 " + numeroClase + " label" + (numeroLabel + 1) +"\n");
			   assemblyCode = assemblyCode.concat("\t move    $s1 $a0\n");
			   assemblyCode = assemblyCode.concat("\t move    $a0 $s1\n");
			   assemblyCode = assemblyCode.concat("\t b    "+ " label" + labelForThis  + "\n");
			   numeroLabel++;
	    	        
	   
	    	}
		   
	    	assemblyCode = assemblyCode.concat("label" + numeroLabel  +":\n");
	    	numeroLabel++;
	    	assemblyCode = assemblyCode.concat("\t jal    _case_abort\n");
			
			assemblyCode = assemblyCode.concat("label"+labelForThis + ":\n");
			labelForThis = numeroLabel;
			numeroLabel++;
			return assemblyCode;
		
	}

	public String solveLetExpr(ALetExpr node){		
		   String assemblyCode ="";
		   LinkedList letdecls = (LinkedList) node.getLetDecl().clone();
		   Iterator a = letdecls.iterator();
		   int sRegCount=1;
		   while (a.hasNext()){
			   ALetDecl letdecl = (ALetDecl) letdecls.poll();
			   PExpr exp = (PExpr)letdecl.getExpr();
			   
			   if  (exp.getClass().toString().contains("AIntExpr")){
				   AIntExpr ai = (AIntExpr) exp;
				   String value = (ai.getIntConst().toString().split(" "))[0];
				   assemblyCode = assemblyCode.concat("\t la      $s" + sRegCount +" int_const"+Integer.toString((intConstCopy.indexOf(value))) + "\n");
				   sRegCount++;
			   }
			   else {		   
				   assemblyCode = assemblyCode.concat(codeGen(exp, false));
			   }
			   VariablesGlobalStack++;
			  // assemblyCode = assemblyCode.concat("\t move $s1 $a0\n");
		   }
		   //assemblyCode = assemblyCode.concat("-----codeAsllleetttexpr\n");
		   assemblyCode = assemblyCode.concat(codeGen(node.getExpr(), false));
		   //assemblyCode = assemblyCode.concat("-----codeAsllleetttexpr\n");
		   /*assemblyCode = assemblyCode.concat(codeGen(node.getTest(),false));
		   int labelForThis = numeroLabel;
		  
		   
		   numeroLabel++;
		   assemblyCode = assemblyCode.concat("\t bne    $a0 $zero label"+ numeroLabel + "\n");
		   assemblyCode = assemblyCode.concat("\t la     $a0 str_const0\n");
		   assemblyCode = assemblyCode.concat("\t li     $t1 1\n");
		   assemblyCode = assemblyCode.concat("\t jal    _case_abort2\n");
		   
		   boolean first= true;
		   
		   LinkedList<PBranch> branches = (LinkedList<PBranch>) node.getBranch().clone();
		   Iterator iteradorBranch = branches.iterator();
	    	
	    	while(iteradorBranch.hasNext())
	    	{
	    		ABranch tmp = (ABranch) branches.poll();
 	        int numeroClase = clases_Numero_Almacenamiento.get(tmp.getTypeId().toString().trim());
 	        
	 		   assemblyCode = assemblyCode.concat("label"+ numeroLabel + ":\n");
	 		   if (first){
	 			   assemblyCode = assemblyCode.concat("\t lw     $t2 0($a0)\n");
	 			   first = false;
	 		   }
			   assemblyCode = assemblyCode.concat("\t blt $t2 " + numeroClase + " label" + (numeroLabel + 1) +"\n");
			   assemblyCode = assemblyCode.concat("\t bgt $t2 " + numeroClase + " label" + (numeroLabel + 1) +"\n");
			   assemblyCode = assemblyCode.concat("\t move    $s1 $a0\n");
			   assemblyCode = assemblyCode.concat("\t move    $a0 $s1\n");
			   assemblyCode = assemblyCode.concat("\t b    "+ " label" + labelForThis  + "\n");
			   numeroLabel++;
	    	        
	   
	    	}
		   
	    	assemblyCode = assemblyCode.concat("label" + numeroLabel  +":\n");
	    	numeroLabel++;
	    	assemblyCode = assemblyCode.concat("\t jal    _case_abort\n");
			
			assemblyCode = assemblyCode.concat("label"+labelForThis + ":\n");
			labelForThis = numeroLabel;
			numeroLabel++;*/
			return assemblyCode;
		
	}
	
	public String solveWhileExpr(AWhileExpr node){		
		   String assemblyCode ="";
		   int labelForThis = numeroLabel;
		   numeroLabel++;
		   assemblyCode = assemblyCode.concat("label"+ labelForThis + ":\n");
		   
		   
		   assemblyCode = assemblyCode.concat(codeGen(node.getTest(),false));
		   
			assemblyCode = assemblyCode.concat("\t lw   $t1 12($a0)\n"); 
			assemblyCode = assemblyCode.concat("\t beq  $t1 $zero label"+ (numeroLabel)+ "\n");
			
			 assemblyCode = assemblyCode.concat(codeGen(node.getLoop(),false));
			
			assemblyCode = assemblyCode.concat("\t b     label" + labelForThis +"\n"); 
			
			// el del true

			labelForThis = numeroLabel;
			numeroLabel++;
			assemblyCode = assemblyCode.concat("label"+labelForThis + ":\n");
			assemblyCode = assemblyCode.concat("\t move    $a0 $zero" + "\n");

			imprimeSalidaMetodo = false;
	
			return assemblyCode;
		
	}
	
	public String solveIsVoidExpr(AIsvoidExpr node){		
		   String assemblyCode ="";
		   assemblyCode = assemblyCode.concat(codeGen(node.getExpr(),false));
		//   String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t beqz   $a0 label"+ numeroLabel + "\n"); 
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
		//	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
			
			
			assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
		//	assemblyCode = assemblyCode.concat("\t lw      $fp 12($sp)" + "\n");
		//	assemblyCode = assemblyCode.concat("\t lw      $s0 8($sp)" + "\n");
		//	assemblyCode = assemblyCode.concat("\t lw      $ra 4($sp)" + "\n");
		//	textlabels = textlabels.concat("\t addiu   $sp $sp 12" + "\n");
		//	textlabels = textlabels.concat("\t jr      $ra" + "\n");
			numeroLabel++;
			return assemblyCode;
	}
	
	public String solveBoolExpr(ABoolExpr node){
		String value = (node.getBoolConst().toString().split(" "))[0];
		String assemblyCode ="";
		if(value.equals("true")){
		//	String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
	//		methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
			
		}
		else {
			//String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
		//	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);			
		}
		return assemblyCode;
	}

	public String solveStrExpr(AStrExpr node){
		//String value = (node.getStrConst().toString().split(" "))[0];
		
		String value = (node.getStrConst().toString());
		//System.out.println("ASFDA" + value);
		//System.out.println("ASFDAindex  / " + stackStringGbl.indexOf(value));
		String assemblyCode = "";
		
		if(stackStringGbl.indexOf(value)>-1){
			//String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t la      $a0 str_const"+ stackStringGbl.indexOf(value) + "\n");
			methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);			
		}
		else if (value.equals("\n")){
			Integer y = stackStringGbl.indexOf("\\n");
			assemblyCode = assemblyCode.concat("\t la      $a0 str_const"+ y + "\n");
			methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);	
		}
		else {
			Stack copia = (Stack) stackStringGbl.clone();
			int contador = copia.size() - 1;
			while (!copia.isEmpty()){
				String r= (String) copia.pop();
				r = r.trim();
				String v = value.trim();
				if (v.equals(r)){
					assemblyCode = assemblyCode.concat("\t la      $a0 str_const"+ contador + "\n");
					methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
				}
				else {
					contador--;
				}
			}
		}
		
		return assemblyCode;
	}

	public String solveObjectExpr(AObjectExpr node, boolean isFirstOperator){
		String value = (node.getObjectId().toString().split(" "))[0];
		// System.out.println("ObjectID" + value);
		String assemblyCode ="";
		if(value.equals("self")){
			//String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			assemblyCode = assemblyCode.concat("\t move    $a0 $s0\n");
			//methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);	
		}
		else {
			Iterator itr = idTable.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry me = (Map.Entry)itr.next();
				Object[] array = (Object[]) me.getValue();
				PExpr exp = (PExpr) array[1];
				String nameVariable = (String) array[0];
				if(nameVariable.equals(value)){
					//YA ENCONTRE LA VARIABLE QUE BUSCABA
					int numeroVariable = (int)me.getKey();
					if(isFirstOperator){
						
						assemblyCode = assemblyCode.concat("\t lw    $s1 "+ (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
					}
					else{
					    assemblyCode = assemblyCode.concat("\t lw    $a0 "+ (12 + ((numeroVariable -1) * 4)) + "($s0)" + "\n");
					}
				}
				
			}
		//	assemblyCode = assemblyCode.concat("\t lw    $a0 "+ (contadorPilaVariables + 12) +"($s0)\n");
		}
		return assemblyCode;
	}

	public String solveLtExpr(ALtExpr node){

        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode="";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		assemblyCode = assemblyCode.concat(codeGen(right,false));
		
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s1)\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
		int labelForThis = numeroLabel;
		assemblyCode = assemblyCode.concat("\t blt     $t1 $t2 label"+ numeroLabel + "\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
		
		
		assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");

		numeroLabel++;
		return assemblyCode;
	}

	public String solveLeExpr(ALeExpr node){

        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode="";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		assemblyCode = assemblyCode.concat(codeGen(right,false));
		
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s"+sRegisterCounter +")\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
		assemblyCode = assemblyCode.concat("\t ble     $t1 $t2 label"+ numeroLabel + "\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
		
		assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");

		numeroLabel++;
		return assemblyCode;
	}
	
	public String solveEqExpr(AEqExpr node){
        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode="";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		assemblyCode = assemblyCode.concat(codeGen(right,false));
		
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s1)\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const1\n");
		assemblyCode = assemblyCode.concat("\t beq     $t1 $t2 label"+ numeroLabel + "\n"); 
		
		assemblyCode = assemblyCode.concat("\t la      $a0 bool_const0\n");
		assemblyCode = assemblyCode.concat("\t jal     equality_test\n");
		
		assemblyCode = assemblyCode.concat("label"+numeroLabel + ":\n");

		numeroLabel++;
		return assemblyCode;
	}
	
	public String solveIntExpr(AIntExpr node, boolean isFirstOperator){		
	    String value = (node.getIntConst().toString().split(" "))[0];
	    
		//String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
	    String assemblyCode ="";
	    
	    if(isFirstOperator){
	    	if (sRegisterCounter!=2){
	    		sRegisterCounter++;
	    	}
			if(intConstCopy.contains(value)){
				assemblyCode = assemblyCode.concat("\t la      $s" + sRegisterCounter +" int_const"+Integer.toString((intConstCopy.indexOf(value))) + "\n");
			}
			else {
				assemblyCode = assemblyCode.concat("\t la      $s" + sRegisterCounter +" "+ value +"\n");
			}		    	
	    }
	    else {
			if(intConstCopy.contains(value)){
				assemblyCode = assemblyCode.concat("\t la      $a0 int_const"+Integer.toString((intConstCopy.indexOf(value))) + "\n");
			}
			else {
				assemblyCode = assemblyCode.concat("\t la      $a0 "+ value +"\n");
			}
	    }
	    
		return assemblyCode;
//		methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
	
}
	public String solvePlusExpr(APlusExpr node){

        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode = "";
		
			assemblyCode = assemblyCode.concat(codeGen(left,true));
			
			assemblyCode = assemblyCode.concat(codeGen(right,false));
		
			assemblyCode = assemblyCode.concat("\t jal Object.copy\n"); 
			assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
			assemblyCode = assemblyCode.concat("\t lw      $t1 12($s1)\n"); 
			assemblyCode = assemblyCode.concat("\t add     $t1 $t1 $t2\n"); 
			assemblyCode = assemblyCode.concat("\t sw      $t1 12($a0)\n");
			assemblyCode = assemblyCode.concat("\t move      $s1 $a0\n");

		return assemblyCode;
	}

	public String solveMinusExpr(AMinusExpr node){

        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode = "";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		
		assemblyCode = assemblyCode.concat(codeGen(right,false));
	
		assemblyCode = assemblyCode.concat("\t jal Object.copy\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s1)\n"); 
		assemblyCode = assemblyCode.concat("\t sub     $t1 $t1 $t2\n"); 
		assemblyCode = assemblyCode.concat("\t sw      $t1 12($a0)\n");
		assemblyCode = assemblyCode.concat("\t move      $s1 $a0\n");
		
		return assemblyCode;


	}
	
	public String solveMultExpr(AMultExpr node){
		
        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode = "";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		sRegisterCounter = 2;
		assemblyCode = assemblyCode.concat(codeGen(right,false));
	
 		assemblyCode = assemblyCode.concat("\t jal Object.copy\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s" + sRegisterCounter +")\n"); 
		assemblyCode = assemblyCode.concat("\t mul     $t1 $t1 $t2\n"); 
		assemblyCode = assemblyCode.concat("\t sw      $t1 12($a0)\n");
		assemblyCode = assemblyCode.concat("\t move      $s" + sRegisterCounter +" $a0\n");
		
		return assemblyCode;
	}
	
	public String solveDivExpr(ADivExpr node){
		
        PExpr right = (PExpr) node.getR();
		PExpr left = (PExpr) node.getL();
		String assemblyCode = "";
		assemblyCode = assemblyCode.concat(codeGen(left,true));
		sRegisterCounter = 2;
		assemblyCode = assemblyCode.concat(codeGen(right,false));
	
		assemblyCode = assemblyCode.concat("\t jal Object.copy\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t2 12($a0)\n"); 
		assemblyCode = assemblyCode.concat("\t lw      $t1 12($s" + sRegisterCounter +")\n"); 
		assemblyCode = assemblyCode.concat("\t div     $t1 $t1 $t2\n"); 
		assemblyCode = assemblyCode.concat("\t sw      $t1 12($a0)\n");
		assemblyCode = assemblyCode.concat("\t move      $s" + sRegisterCounter +" $a0\n");
		
		return assemblyCode;
	}
	
    public void outAMethodFeature(AMethodFeature node)
    {
    		sRegisterCounter = 0;
    		if (currentClassName.equals("Main")&&currentMethodName.trim().equals("main")){
    			textMain_main = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
		        int numeroParametros = node.getFormal().size();
		    	textMain_main = textMain_main.concat("\t lw      $fp "+ ((12 + ((VariablesGlobalStack -1) * 4))) + "($sp)" + "\n");
		    	textMain_main = textMain_main.concat("\t lw      $s0 "+ ((12 + ((VariablesGlobalStack -1) * 4))-4) + "($sp)" + "\n");
		    	textMain_main = textMain_main.concat("\t lw      $ra "+ ((12 + ((VariablesGlobalStack -1) * 4))-8) + "($sp)" + "\n");
		    	textMain_main = textMain_main.concat("\t addiu   $sp $sp "+ (((12 + ((VariablesGlobalStack -1) * 4))) + (numeroParametros * 4)) + "\n");
		    	textMain_main = textMain_main.concat("\t jr      $ra" + "\n");
		    	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, textMain_main);
	    	   
    		}
    		else {
    			
		    		String assemblyCode = methodsName_AssemblerCode_Table.get(currentClassName + "." + currentMethodName);
			        int numeroParametros = node.getFormal().size();
			    	assemblyCode = assemblyCode.concat("\t lw      $fp "+ ((12 + ((VariablesGlobalStack -1) * 4))) + "($sp)" + "\n");
			    	assemblyCode = assemblyCode.concat("\t lw      $s0 "+ ((12 + ((VariablesGlobalStack -1) * 4))-4) + "($sp)" + "\n");
			    	assemblyCode = assemblyCode.concat("\t lw      $ra "+ ((12 + ((VariablesGlobalStack -1) * 4))-8) + "($sp)" + "\n");
			    	assemblyCode = assemblyCode.concat("\t addiu   $sp $sp "+ (((12 + ((VariablesGlobalStack -1) * 4))) + (numeroParametros * 4)) + "\n");
			    	assemblyCode = assemblyCode.concat("\t jr      $ra" + "\n");
			    	methodsName_AssemblerCode_Table.put(currentClassName + "." + currentMethodName, assemblyCode);
		    	    text10_methods = text10_methods.concat(assemblyCode);
    		}
    }
    
    public void outAProgram(AProgram node){
    	out.print(text1_1_classNameTab);
    	out.print(text1_basicDeclarations);
    	out.print(text2_dynamic_dispTab);
    	out.print(text3_static_dispTab);
    	out.print(text4_dynamic_protObj);
    	out.print(text5_static_protObj);
    	out.print(text4_1_Main_protObj);
    	out.print(text6_heap_start);
    	out.print(text7_obj_init);
    	out.print(text8_dynamic_init);
    	out.print(text9_static_init);
    	out.print(text10_methods);
    	out.print(textMain_main);
    	//out.print(textlabels);
    }

}
