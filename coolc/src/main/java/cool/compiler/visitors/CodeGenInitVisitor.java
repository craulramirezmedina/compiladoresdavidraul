package cool.compiler.visitors;

import java.io.PrintStream;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;


import cool.compiler.autogen.analysis.DepthFirstAdapter;
import cool.compiler.autogen.node.AAttributeFeature;
import cool.compiler.autogen.node.ABoolExpr;
import cool.compiler.autogen.node.ABranch;
import cool.compiler.autogen.node.ACaseExpr;
import cool.compiler.autogen.node.AClassDecl;
import cool.compiler.autogen.node.ADivExpr;
import cool.compiler.autogen.node.AEqExpr;
import cool.compiler.autogen.node.AFormal;
import cool.compiler.autogen.node.AIfExpr;
import cool.compiler.autogen.node.AIntExpr;
import cool.compiler.autogen.node.AIsvoidExpr;
import cool.compiler.autogen.node.ALeExpr;
import cool.compiler.autogen.node.ALetDecl;
import cool.compiler.autogen.node.ALetExpr;
import cool.compiler.autogen.node.ALtExpr;
import cool.compiler.autogen.node.AMethodFeature;
import cool.compiler.autogen.node.AMinusExpr;
import cool.compiler.autogen.node.AMultExpr;
import cool.compiler.autogen.node.ANegExpr;
import cool.compiler.autogen.node.ANewExpr;
import cool.compiler.autogen.node.ANotExpr;
import cool.compiler.autogen.node.AObjectExpr;
import cool.compiler.autogen.node.APlusExpr;
import cool.compiler.autogen.node.AProgram;
import cool.compiler.autogen.node.AStrExpr;
import cool.compiler.autogen.node.PExpr;
import cool.compiler.autogen.node.PFeature;
import cool.compiler.autogen.node.PFormal;
import cool.compiler.autogen.node.Start;
import cool.compiler.ref.symbols.AbstractSymbol;
import cool.compiler.ref.tables.AbstractTable;
import cool.compiler.symbols.*;
import cool.compiler.tables.ClassTable;
import cool.compiler.tables.SymbolTableN;

public class CodeGenInitVisitor extends DepthFirstAdapter{

	/*
	 * CodeGenVisitor
	*/
    Map<String, String> heredaMap;
    Map<String, LinkedList<String>> className_MethodsName;
    TreeMap<String, TreeMap<String,String>> className_AttributesName;
    TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta;

    String currentClassName;
  

    // Constructor
    public CodeGenInitVisitor(TreeMap<String, String> map, TreeMap<String, LinkedList<String>> cl, TreeMap<String, TreeMap<String, String>> className_AttributesName2, TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta) {
    	heredaMap = map;
    	className_MethodsName = cl;
    	this.className_AttributesName = className_AttributesName2;
    	this.className_MethodsNameCompleta = className_MethodsNameCompleta;
	}

	@Override
    public void inAProgram(AProgram m){
	
		className_MethodsNameCompleta.put("Object", new TreeMap<String, LinkedList<String>>());
		TreeMap objectMap = className_MethodsNameCompleta.get("Object");
		objectMap.put("Object", new LinkedList<String>());
		LinkedList objectMethods = (LinkedList) objectMap.get("Object");
		objectMethods.push("abort"); 
		objectMethods.push("type_name"); 
		objectMethods.push("copy"); 
		objectMap.put("Object", objectMethods);
		className_MethodsNameCompleta.put("Object", objectMap);
		
		
		
	    heredaMap.put("IO", "Object");

	    heredaMap.put("Int", "Object");

	    heredaMap.put("Bool", "Object");

	    heredaMap.put("String", "Object");
	    
	    className_MethodsName.put("Object", new LinkedList<String>());
	    className_AttributesName.put("Object", new TreeMap<String,String>());
	    LinkedList<String> a = className_MethodsName.get("Object");
    	a.push("abort"); 
    	a.push("type_name"); 
    	a.push("copy"); 
    	
	    className_MethodsName.put("String", new LinkedList<String>());
	    className_AttributesName.put("String", new TreeMap<String,String>());
	    LinkedList<String> st = className_MethodsName.get("String");
    	st.push("length"); 
    	st.push("concat"); 
    	st.push("substr"); 
    	
    	className_MethodsName.put("Bool", new LinkedList<String>());
    	className_AttributesName.put("Bool", new TreeMap<String,String>());
    	
    	className_MethodsName.put("Int", new LinkedList<String>());
    	className_AttributesName.put("Int", new TreeMap<String,String>());
    	
    	className_MethodsName.put("IO", new LinkedList<String>());
    	className_AttributesName.put("IO", new TreeMap<String,String>());
    	LinkedList<String> io = className_MethodsName.get("IO");
     	io.push("out_string"); 
     	io.push("out_int"); 
     	io.push("in_string");
     	io.push("in_in");

    }
    
    @Override
    //Hay q hacer un open scope donde recidira la asociacion de la variable definida
    public void inAClassDecl(AClassDecl n){
    	
    	
    	currentClassName = (n.getName().toString().split(" "))[0];
    	
    	String hereda = n.getInherits().toString().trim();
    	heredaMap.put(currentClassName, hereda);
    	
    	className_MethodsName.put(currentClassName.trim(), new LinkedList<String>());
    	className_AttributesName.put(currentClassName.trim(), new TreeMap<String,String>());
    	
    }
    
    @Override 
    public void inAMethodFeature(AMethodFeature n){
    	LinkedList<String> a = className_MethodsName.get(currentClassName);
    	a.push(n.getObjectId().toString().trim());  
    	
    }
    @Override 
    public void inAAttributeFeature(AAttributeFeature n){
    	TreeMap<String,String> a = (TreeMap<String, String>) className_AttributesName.get(currentClassName);
    	a.put(n.getObjectId().toString().trim(), n.getTypeId().toString().trim()); 
    	
    }
}
