package cool.compiler.visitors;

import java.io.PrintStream;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;


import cool.compiler.autogen.analysis.DepthFirstAdapter;
import cool.compiler.autogen.node.AAttributeFeature;
import cool.compiler.autogen.node.ABoolExpr;
import cool.compiler.autogen.node.ABranch;
import cool.compiler.autogen.node.ACaseExpr;
import cool.compiler.autogen.node.AClassDecl;
import cool.compiler.autogen.node.ADivExpr;
import cool.compiler.autogen.node.AEqExpr;
import cool.compiler.autogen.node.AFormal;
import cool.compiler.autogen.node.AIfExpr;
import cool.compiler.autogen.node.AIntExpr;
import cool.compiler.autogen.node.AIsvoidExpr;
import cool.compiler.autogen.node.ALeExpr;
import cool.compiler.autogen.node.ALetDecl;
import cool.compiler.autogen.node.ALetExpr;
import cool.compiler.autogen.node.ALtExpr;
import cool.compiler.autogen.node.AMethodFeature;
import cool.compiler.autogen.node.AMinusExpr;
import cool.compiler.autogen.node.AMultExpr;
import cool.compiler.autogen.node.ANegExpr;
import cool.compiler.autogen.node.ANewExpr;
import cool.compiler.autogen.node.ANotExpr;
import cool.compiler.autogen.node.AObjectExpr;
import cool.compiler.autogen.node.APlusExpr;
import cool.compiler.autogen.node.AProgram;
import cool.compiler.autogen.node.AStrExpr;
import cool.compiler.autogen.node.PExpr;
import cool.compiler.autogen.node.PFeature;
import cool.compiler.autogen.node.PFormal;
import cool.compiler.autogen.node.Start;
import cool.compiler.ref.symbols.AbstractSymbol;
import cool.compiler.ref.tables.AbstractTable;
import cool.compiler.symbols.*;
import cool.compiler.tables.ClassTable;
import cool.compiler.tables.SymbolTableN;

public class CodeGenInitClassMethodsAttributesVisitor extends DepthFirstAdapter{

    Map<String, String> heredaMap;
    Map<String, LinkedList<String>> className_MethodsName;
    TreeMap<String, TreeMap<String,String>> className_AttributesName;
    TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta;
    String currentClassName;
  

    // Constructor
    public CodeGenInitClassMethodsAttributesVisitor(TreeMap<String, String> map, TreeMap<String, LinkedList<String>> cl, TreeMap<String, TreeMap<String, String>> className_AttributesName2, TreeMap<String, TreeMap<String, LinkedList<String>>> className_MethodsNameCompleta) {
    	heredaMap = map;
    	className_MethodsName = cl;
    	this.className_AttributesName = className_AttributesName2;
    	this.className_MethodsNameCompleta = className_MethodsNameCompleta;
	}

    @Override
    //Hay q hacer un open scope donde recidira la asociacion de la variable definida
    public void inAProgram(AProgram n){
    	llenaClasesBasicas("IO");
    	llenaClasesBasicas("Int");
    	llenaClasesBasicas("String");
    	llenaClasesBasicas("Bool");
    	
    }
    @Override
    //Hay q hacer un open scope donde recidira la asociacion de la variable definida
    public void inAClassDecl(AClassDecl n){
    	
    	currentClassName = (n.getName().toString().split(" "))[0];
    	   
   		className_MethodsNameCompleta.put(currentClassName, new TreeMap<String, LinkedList<String>>());
   		TreeMap ioMap = className_MethodsNameCompleta.get(currentClassName);
   		    
   		
    		//Primero obtengo todas las clases de las que hereda
    		Stack<String> pilaHereda = new Stack<String>();
    		String clasetmp;
    		String heredatmp;
    		clasetmp = currentClassName.trim();
    		
    		while (!clasetmp.equals("Object")){
	    		heredatmp= heredaMap.get(clasetmp).trim();
	    //		if(!heredatmp.equals("Object")){
	    			pilaHereda.push(heredaMap.get(clasetmp));
	    	//	}
	    		clasetmp = heredatmp.trim();
    		}
    		
    		while (!pilaHereda.isEmpty()){
    			
    			String padre = pilaHereda.pop();
	    		TreeMap<String, LinkedList<String>> registroPadre = className_MethodsNameCompleta.get(padre);
	    		
	    		Iterator itrMetodosPadre = registroPadre.entrySet().iterator();;
				while (itrMetodosPadre.hasNext()) {
					Map.Entry me = (Map.Entry)itrMetodosPadre.next();
					ioMap.put(me.getKey(), me.getValue());
				}
    			
    			
	    		LinkedList methodsPadre = (LinkedList) className_MethodsName.get(padre).clone();
	    		LinkedList methodsThisClass = (LinkedList) className_MethodsName.get(currentClassName.trim()).clone();
	    		TreeMap<String, String> atributosPadre = (TreeMap)className_AttributesName.get(padre).clone();
	    		TreeMap atributosThisClass = (TreeMap)className_AttributesName.get(currentClassName.trim()).clone();
	    		
	    		Iterator a = methodsPadre.iterator();
	        	
	        	while(a.hasNext())
	        	{
	        	        String methodName = (String) methodsPadre.poll();
	        	        methodsThisClass.push(methodName); 
	        	 }
	        	
	        	 
	        	 className_MethodsName.put(currentClassName.trim(),methodsThisClass);
	        	 
	        	 for (Map.Entry<String, String> entry : atributosPadre.entrySet())
	        	 {
	        		 String atributeName = (String) entry.getKey();
	        		 atributosThisClass.put(atributeName, entry.getValue());
	        	 }
	        	 className_AttributesName.put(currentClassName.trim(),atributosThisClass);

    	         }
 
    		
        	LinkedList<PFeature> lkn = n.getFeature();
        	LinkedList<PFeature> lk = (LinkedList<PFeature>) lkn.clone();
        	Iterator iterador = lk.iterator();
        	LinkedList methodsThisClass = (LinkedList) className_MethodsName.get(currentClassName.trim()).clone();
        	ioMap.put(currentClassName, new LinkedList());
        	LinkedList tmpForThis = (LinkedList) ioMap.get(currentClassName);
        	
    	while(iterador.hasNext())
    	{
    	        PFeature o = lk.peek();
    	        if (o.getClass().toString().contains("AMethodFeature")){
    	        	AMethodFeature m = (AMethodFeature)lk.poll();
    	        	methodsThisClass.push(m.getObjectId().toString().trim()); 
    	        	tmpForThis.push(m.getObjectId().toString().trim()); 
    	        }    
    	        else if (o.getClass().toString().contains("AttributeFeature")) {
    	        	AAttributeFeature m = (AAttributeFeature)lk.poll();
    	        	Object[] tmp = {m.getObjectId().toString().trim(), m.getExpr(), m.getTypeId().toString() };
    	        } 
    	}
    		ioMap.put(currentClassName, tmpForThis);
    	
    	className_MethodsNameCompleta.put(currentClassName, ioMap);
    	
    }

    public void llenaClasesBasicas(String className){
    

    	
 	   currentClassName = className;

		
		className_MethodsNameCompleta.put(currentClassName, new TreeMap<String, LinkedList<String>>());
		TreeMap ioMap = className_MethodsNameCompleta.get(currentClassName);
		     
		
		
 		//Si no hereda de Object, hay que cargarle los metodos de TODAS las clases que hereda.
 		
 		//Primero obtengo todas las clases de las que hereda
 		Stack<String> pilaHereda = new Stack<String>();
 		String clasetmp;
 		String heredatmp;
 		clasetmp = currentClassName.trim();
 		
 		while (!clasetmp.equals("Object")){
	    		heredatmp= heredaMap.get(clasetmp).trim();
	    			pilaHereda.push(heredaMap.get(clasetmp));
	    		clasetmp = heredatmp.trim();
 		}
 		while (!pilaHereda.isEmpty()){
	    		String padre = pilaHereda.pop();
	    		TreeMap<String, LinkedList<String>> registroPadre = className_MethodsNameCompleta.get(padre);
	    		
	    		Iterator itrMetodosPadre = registroPadre.entrySet().iterator();;
				while (itrMetodosPadre.hasNext()) {
					Map.Entry me = (Map.Entry)itrMetodosPadre.next();
					ioMap.put(me.getKey(), me.getValue());
				}
			     /////
	    		LinkedList methodsPadre = (LinkedList) className_MethodsName.get(padre).clone();
	    		LinkedList methodsThisClass = (LinkedList) className_MethodsName.get(currentClassName.trim()).clone();
	    		TreeMap<String, String> atributosPadre = (TreeMap)className_AttributesName.get(padre).clone();
	    		TreeMap atributosThisClass = (TreeMap)className_AttributesName.get(currentClassName.trim()).clone();
	    		
	    		Iterator a = methodsPadre.iterator();
	        	
	        	while(a.hasNext())
	        	{
	        	        String methodName = (String) methodsPadre.poll();
	        	        methodsThisClass.push(methodName); 
	
	        	 }
	        	 className_MethodsName.put(currentClassName.trim(),methodsThisClass);
	        	 
	        	 for (Map.Entry<String, String> entry : atributosPadre.entrySet())
	        	 {
	        		 String atributeName = (String) entry.getKey();
	        		 atributosThisClass.put(atributeName, entry.getValue());
	        	 }
	        	 className_AttributesName.put(currentClassName.trim(),atributosThisClass);

 	}
 		if(currentClassName.equals("String")){
 			ioMap.put("String", new LinkedList<String>());
 			LinkedList<String> st = (LinkedList<String>) ioMap.get("String");
 			st.push("length"); 
 	    	st.push("concat"); 
 	    	st.push("substr"); 
 	    	ioMap.put("String", st);
 		}
 		else if(currentClassName.equals("IO")){
 			ioMap.put("IO", new LinkedList<String>());
 			LinkedList<String> a = (LinkedList<String>) ioMap.get("IO");
 	     	a.push("out_string"); 
 	     	a.push("out_int"); 
 	     	a.push("in_string");
 	     	a.push("in_in");
 	     	ioMap.put("IO", a);
 		}
 		className_MethodsNameCompleta.put(currentClassName, ioMap);
    }
}
