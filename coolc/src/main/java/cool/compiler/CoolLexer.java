package cool.compiler;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PushbackReader;
import java.lang.reflect.Method;

import cool.compiler.autogen.lexer.Lexer;
import cool.compiler.autogen.lexer.LexerException;
import cool.compiler.autogen.node.EOF;
import cool.compiler.autogen.node.TCommentBegin;
import cool.compiler.autogen.node.TCommentEnd;
import cool.compiler.autogen.node.TCommentNest;
import cool.compiler.autogen.node.TErrend;
import cool.compiler.autogen.node.TErrorStarLpar;
import cool.compiler.autogen.node.TStrBegin;
import cool.compiler.autogen.node.TStrConst;
import cool.compiler.autogen.node.TStrEnd;
import cool.compiler.autogen.node.TStrErrOutDq;
import cool.compiler.autogen.node.TStrErrOutNl;
import cool.compiler.autogen.node.TStrErrorEol;
import cool.compiler.autogen.node.TStrErrorEscapedNull;
import cool.compiler.autogen.node.TStrErrorNull;
import cool.compiler.autogen.node.Token;
import cool.compiler.autogen.parser.Parser;
import cool.compiler.util.Messages;
import cool.compiler.util.Util;

public class CoolLexer extends Lexer {
	private Parser parser;
	private Method index;
	private PrintStream out;
	private Token lastToken;

	public CoolLexer(PushbackReader in, PrintStream out) {
		super(in);
		this.out = out;
	}
	
	public Token getLastToken() {
		return lastToken;
	}
	
	boolean ignore(Token t) {
		try {
			if ((int) ((Integer) index.invoke(parser, t)) == -1) {
				return true;
			}
		} catch (Exception e) {
			/* Never actually reached */
		}
		
		return false;
	}

	int comments = 0;
	boolean inStr = false, jumpOne = false, errorNulInStr = false;
	TStrConst tempStr = null;

	protected void checks() {
		// Comentarios anidados.
		if (token instanceof TCommentBegin) {
			comments++;
		}

		else if (token instanceof TCommentNest) {
			comments++;
		} 

		else if (token instanceof TCommentEnd) {
			comments--;
			if (comments == 0) {
				state = State.INITIAL;
			} else {
				state = State.COMMENT;
			}
		} 

			
		// Comienzo de string
		else if (token instanceof TStrBegin) {
			inStr = true;
		}
			
		// Composici�n final del string
		else if (token instanceof TStrEnd) {
			if (inStr) {
				if (tempStr == null) {
					token = new TStrConst("", token.getLine(), token.getPos()); //$NON-NLS-1$
				} else {
					tempStr.setText(unescape(tempStr.getText()));
					token = tempStr;
				}
				if (token.getText().length() > 1024) {
					out.format(Messages.getString("CoolLexer.1"), token.getLine()); //$NON-NLS-1$
					jumpOne = true;
				}
				if (errorNulInStr) {
					out.format(Messages.getString("CoolLexer.2"), //$NON-NLS-1$
							token.getLine());
					errorNulInStr = false;
					jumpOne = true;					
				}
				tempStr = null;
				inStr = false;
			}
		}

		// Acumulaci�n en strings intermedios del string
		else if (token instanceof TStrConst) {
			if (tempStr == null) {
				tempStr = (TStrConst) token;
			} else {
				tempStr.setText(tempStr.getText() + token.getText());
			}
		}

		// Errores en medio del string
		else if (token instanceof TStrErrorEol) {
			out.format(Messages.getString("CoolLexer.3"), token.getLine()); //$NON-NLS-1$
			tempStr = null;
			inStr = false;
			jumpOne = true;			
		}

		else if (token instanceof TStrErrorNull) {
			out.format(Messages.getString("CoolLexer.4"), token.getLine()); //$NON-NLS-1$
		}
		
		else if (token instanceof TStrErrorEscapedNull) {
			out.format(Messages.getString("CoolLexer.5"), token.getLine()); //$NON-NLS-1$
		} 
		else if (token instanceof TStrErrOutNl) {
			tempStr = null;
			inStr = false;
			jumpOne = true;
		}
		else if (token instanceof TStrErrOutDq) {
			tempStr = null;
			inStr = false;
			jumpOne = true;
		}


		// Errores por fin de archivo
		else if (token instanceof EOF) {
			if (inStr) {
				out.format(Messages.getString("CoolLexer.6"), //$NON-NLS-1$
						token.getLine());
				inStr = false;
			}
			if (comments > 0) {
				out.format(Messages.getString("CoolLexer.7"), token.getLine()); //$NON-NLS-1$
				comments = 0;
			}
		}
		
		// Error por fin de comentario encontrado		
		else if (token instanceof TErrorStarLpar) {
			out.format(Messages.getString("CoolLexer.8"), token.getLine()); //$NON-NLS-1$
			jumpOne = true;
		} 
		
		// Error sobre cualquier token extra�o al final
		else if (token instanceof TErrend) {
			out.format(Messages.getString("CoolLexer.9"), token.getLine(), Util.escapeString(token.getText())); //$NON-NLS-1$
			jumpOne = true;
		}
	}
	
	@Override
	public Token next() throws LexerException, IOException {
		lastToken = token;
		return super.next();
	}

	@Override
	protected void filter() throws LexerException, IOException {		
		checks();
		while (ignore(token) || inStr || jumpOne) {
			token = getToken();
			jumpOne = false;
			checks();
		}
	}

	public void setParser(Parser p) {
		parser = p;
		/* Ni�os, no hagan esto sin la supervisi�n de un adulto */

		try {
			for (Method m : parser.getClass().getDeclaredMethods()) {
				if ("index".equals(m.getName())) { //$NON-NLS-1$
					m.setAccessible(true);
					this.index = m;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public String unescape(String s) {
		StringBuffer b = new StringBuffer();
		
		for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);        

            if (c == '\\') {
                switch(s.charAt(i+1)) {
                    case 'b':
                        i++;
                        b.append("\b"); //$NON-NLS-1$
                        break;
                    case 't':
                        i++;
                        b.append("\t"); //$NON-NLS-1$
                        break;
                    case 'n':
                        i++;
                        b.append("\n"); //$NON-NLS-1$
                        break;
                    case 'f':
                        i++;
                        b.append("\f"); //$NON-NLS-1$
                        break;
                    case '\\':
                        i++;
                        b.append("\\"); //$NON-NLS-1$
                        break;
                }
            } else {
                b.append(c);
            }
        }

        return b.toString();		
	}

}
