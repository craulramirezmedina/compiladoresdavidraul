package cool.compiler.tables;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.LinkedList;

import cool.compiler.autogen.node.AAttributeFeature;
import cool.compiler.autogen.node.AClassDecl;
import cool.compiler.autogen.node.AFormal;
import cool.compiler.autogen.node.AMethodFeature;
import cool.compiler.autogen.node.ANoExpr;
import cool.compiler.autogen.node.PFeature;
import cool.compiler.autogen.node.PFormal;
import cool.compiler.autogen.node.TObjectId;
import cool.compiler.autogen.node.TTypeId;
import cool.compiler.symbols.AbstractSymbol;
import cool.compiler.symbols.StringSymbol;

/**
 * This class may be used to contain the semantic information such as the
 * inheritance graph. You may use it or not as you like: it is only here to
 * provide a container for the supplied methods.
 */
public class ClassTable {
	private int semantErrors;
	private PrintStream errorStream;
	private Hashtable<AbstractSymbol, AClassDecl> table;
	
	private static ClassTable singleton = new ClassTable( new Hashtable<AbstractSymbol, AClassDecl>());

	private ClassTable(Hashtable<AbstractSymbol, AClassDecl> table){
		this.setTable(table);
		installBasicClasses();
	}
	
	public static ClassTable getInstance( ) {
		return singleton;
	}
	
	public Hashtable<AbstractSymbol, AClassDecl> getTable() {
		return table;
	}

	
	public void setTable(Hashtable<AbstractSymbol, AClassDecl> table) {
		this.table = table;
	}

	/**
	 * Creates data structures representing basic Cool classes (Object, IO, Int,
	 * Bool, String). Please note: as is this method does not do anything
	 * useful; you will need to edit it to make if do what you want.
	 * */
	private void installBasicClasses() {
		AbstractSymbol filename = AbstractTable.stringtable.addString("<basic class>");

		// The following demonstrates how to create dummy parse trees to
		// refer to basic Cool classes. There's no need for method
		// bodies -- these are already built into the runtime system.

		// IMPORTANT: The results of the following expressions are
		// stored in local variables. You will want to do something
		// with those variables at the end of this method to make this
		// code meaningful.

		// The Object class has no parent class. Its methods are
		// cool_abort() : Object aborts the program
		// type_name() : Str returns a string representation
		// of class name
		// copy() : SELF_TYPE returns a copy of the object
		LinkedList<PFeature> featList;
		LinkedList<PFormal> formalList;
		PFeature pf;
		
		featList = new LinkedList<PFeature>();
		
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.cool_abort.getString()),
				new LinkedList<PFormal>(),
				new TTypeId(TreeConstants.Object_.getString()),
				new ANoExpr()
				);		
		featList.add(pf);
		
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.type_name.getString()),
				new LinkedList<PFormal>(),
				new TTypeId(TreeConstants.Str.getString()),
				new ANoExpr()
				);
		featList.add(pf);
		
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.copy.getString()),
				new LinkedList<PFormal>(),
				new TTypeId(TreeConstants.SELF_TYPE.getString()),
				new ANoExpr()
				);
		featList.add(pf);
		
		AClassDecl ObjectClass = new AClassDecl(
				new TTypeId(TreeConstants.Object_.getString()),
				new TTypeId(TreeConstants.No_class.getString()),
				featList
				);
		
		// The IO class inherits from Object. Its methods are
		// out_string(Str) : SELF_TYPE writes a string to the output
		// out_int(Int) : SELF_TYPE "    an int    " "     "
		// in_string() : Str reads a string from the input
		// in_int() : Int "   an int     " "     "
		featList = new LinkedList<PFeature>();
		
		formalList = new LinkedList<PFormal>();		
		formalList.add(new AFormal(new TObjectId(TreeConstants.arg.toString()), new TTypeId(TreeConstants.Str.toString())));		
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.out_string.getString()),
				formalList,
				new TTypeId(TreeConstants.SELF_TYPE.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		formalList = new LinkedList<PFormal>();		
		formalList.add(new AFormal(new TObjectId(TreeConstants.arg.toString()), new TTypeId(TreeConstants.Int.toString())));
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.out_int.getString()),
				formalList,
				new TTypeId(TreeConstants.SELF_TYPE.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		pf = new AMethodFeature(
				new TObjectId(TreeConstants.in_string.getString()),
				formalList,
				new TTypeId(TreeConstants.Str.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		pf = new AMethodFeature(
				new TObjectId(TreeConstants.in_int.getString()),
				formalList,
				new TTypeId(TreeConstants.Int.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		AClassDecl IOClass = new AClassDecl(
				new TTypeId(TreeConstants.IO.getString()),
				new TTypeId(TreeConstants.Object_.getString()),
				featList
				);


		// The Int class has no methods and only a single attribute, the
		// "val" for the integer.
		featList = new LinkedList<PFeature>();
		pf = new AAttributeFeature(
				new TObjectId(TreeConstants.val.getString()),
				new TTypeId(TreeConstants.prim_slot.getString()),
				new ANoExpr()
				);		
		featList.add(pf);
		
		AClassDecl IntClass = new AClassDecl(
				new TTypeId(TreeConstants.Int.getString()),
				new TTypeId(TreeConstants.Object_.getString()),
				featList
				);

		// Bool also has only the "val" slot.
		featList = new LinkedList<PFeature>();
		pf = new AAttributeFeature(
				new TObjectId(TreeConstants.val.getString()),
				new TTypeId(TreeConstants.prim_slot.getString()),
				new ANoExpr()
				);		
		featList.add(pf);
		
		AClassDecl BoolClass = new AClassDecl(
				new TTypeId(TreeConstants.Int.getString()),
				new TTypeId(TreeConstants.Object_.getString()),
				featList
				);

		// The class Str has a number of slots and operations:
		// val the length of the string
		// str_field the string itself
		// length() : Int returns length of the string
		// concat(arg: Str) : Str performs string concatenation
		// substr(arg: Int, arg2: Int): Str substring selection
		featList = new LinkedList<PFeature>();
		pf = new AAttributeFeature(
				new TObjectId(TreeConstants.val.getString()),
				new TTypeId(TreeConstants.Int.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		pf = new AAttributeFeature(
				new TObjectId(TreeConstants.str_field.getString()),
				new TTypeId(TreeConstants.prim_slot.getString()),
				new ANoExpr()
				);		
		featList.add(pf);
		
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.length.getString()),
				new LinkedList<PFormal>(),
				new TTypeId(TreeConstants.Int.getString()),
				new ANoExpr()
				);
		featList.add(pf);		

		formalList = new LinkedList<PFormal>();
		formalList.add(new AFormal(new TObjectId(TreeConstants.arg.toString()), new TTypeId(TreeConstants.Str.toString())));
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.concat.getString()),
				formalList,
				new TTypeId(TreeConstants.Str.getString()),
				new ANoExpr()
				);		
		featList.add(pf);

		formalList = new LinkedList<PFormal>();
		formalList.add(new AFormal(new TObjectId(TreeConstants.arg.toString()), new TTypeId(TreeConstants.Int.toString())));
		formalList.add(new AFormal(new TObjectId(TreeConstants.arg2.toString()), new TTypeId(TreeConstants.Int.toString())));
		pf = new AMethodFeature(
				new TObjectId(TreeConstants.substr.getString()),
				formalList,
				new TTypeId(TreeConstants.Str.getString()),
				new ANoExpr()
				);		
		featList.add(pf);
		
		
		AClassDecl StringClass = new AClassDecl(
				new TTypeId(TreeConstants.Int.getString()),
				new TTypeId(TreeConstants.Object_.getString()),
				featList
				);
		

		table.put(TreeConstants.Object_, ObjectClass);
		table.put(TreeConstants.IO, IOClass);
		table.put(TreeConstants.Int, IntClass);
		table.put(TreeConstants.Bool, BoolClass);
		table.put(TreeConstants.Str, StringClass);	
		/*
		 * Do somethind with Object_class, IO_class, Int_class, Bool_class, and
		 * Str_class here 
		 * 
		 */
		
		/*
		 * Nota Abdul: This is what I would do:
		 */
//		table.put(TreeConstants.Object_, Object_class);
//		table.put(TreeConstants.IO, IO_class);
//		table.put(TreeConstants.Int, Int_class);
//		table.put(TreeConstants.Bool, Bool_class);
//		table.put(TreeConstants.Str, Str_class);
//		
//		table.put(TreeConstants.SELF_TYPE, new class_c(0, TreeConstants.SELF_TYPE, TreeConstants.No_class, null, null));

	}

	public void addClass(String name, AClassDecl n){
		
		table.put(new StringSymbol(name), n);
		
	}
	
	/**
	 * Increments semantic error count and returns the print stream for error
	 * messages.
	 * 
	 * @return a print stream to which the error message is to be printed.
	 * 
	 * */
	public PrintStream semantError() {
		semantErrors++;
		return errorStream;
	}

	/** Returns true if there are any static semantic errors. */
	public boolean errors() {
		return semantErrors != 0;
	}
	
}
