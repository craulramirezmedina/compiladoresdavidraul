package cool.compiler.tables;

import java.util.Hashtable;

public class SymbolTableN {

	String className;
	Hashtable<String, Hashtable<String,String>> methodFeatures;
	Hashtable<String, String> attributeFeatures;
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Hashtable<String, Hashtable<String, String>> getMethodFeatures() {
		return methodFeatures;
	}
	public void setMethodFeatures(Hashtable<String, Hashtable<String, String>> methodFeatures) {
		this.methodFeatures = methodFeatures;
	}
	public Hashtable<String, String> getAttributeFeatures() {
		return attributeFeatures;
	}
	public void setAttributeFeatures(Hashtable<String, String> attributeFeatures) {
		this.attributeFeatures = attributeFeatures;
	}
	
	
}
