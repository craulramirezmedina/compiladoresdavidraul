package cool.compiler.symbols;

public interface Visitor {	
	public void visit(IdSymbol s);
	public void visit(IntSymbol s);
	public void visit(StringSymbol s);
}
