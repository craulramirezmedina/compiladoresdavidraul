package cool.compiler.graph;

import java.util.Stack;

public class RegistroHeredan {

	String nombre;
	private Stack<String> heredan;
	
	public RegistroHeredan(String nombre, Stack<String> heredan) {
		super();
		this.nombre = nombre;
		this.heredan = heredan;
	}

	/**
	 * @return the heredan
	 */
	public Stack<String> getHeredan() {
		return heredan;
	}

	/**
	 * @param heredan the heredan to set
	 */
	public void setHeredan(Stack<String> heredan) {
		this.heredan = heredan;
	}
	
	
}
