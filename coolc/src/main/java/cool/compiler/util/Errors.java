package cool.compiler.util;

import java.io.PrintStream;

public class Errors {
	private static Errors instance;
	private int semantic;
	private PrintStream out;
	
	private Errors() {
	}
	
	public int getSemantic() {
		return semantic;
	}
	
	public void setOut(PrintStream _out) {
		out = _out;
	}
	
	public static Errors getInstance() {
		if (instance == null) {
			instance = new Errors();
		}
		
		return instance;
	}
	
	public void semantic(String msg, Object ... args) {
		semantic++;
		if (out != null) {
			out.format(Messages.getString(msg), args);
		}
	}
	
	public void fatal(String msg, Object ... args) {
		if (out != null) {
			out.format(Messages.getString(msg), args);
		}
		System.exit(1);
	}
	
	public void reset() {
		semantic = 0;
	}
}
