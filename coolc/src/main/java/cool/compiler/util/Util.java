package cool.compiler.util;


public class Util {
	public static String escapeString(String s) {
		StringBuffer b = new StringBuffer();
		
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '\\':
				b.append("\\\\");
				break;
			case '\"':
				b.append("\\\"");
				break;
			case '\n':
				b.append("\\n");
				break;
			case '\t':
				b.append("\\t");
				break;
			case '\b':
				b.append("\\b");
				break;
			case '\f':
				b.append("\\f");
				break;
			default:
				if (c >= 0x20 && c <= 0x7f) {
					b.append(c);
				} else {
					String octal = Integer.toOctalString(c);
					b.append('\\');
					switch (octal.length()) {
					case 1:
						b.append('0');
					case 2:
						b.append('0');
					default:
						b.append(octal);
					}
				}
			}			
		}
		return b.toString();
	}
	

	public static void fatalError(String msg) {
		(new Throwable(msg)).printStackTrace();
		System.exit(1);
	}

}
