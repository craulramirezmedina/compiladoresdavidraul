/* This file was generated by SableCC (http://www.sablecc.org/). */

package cool.compiler.autogen.node;

import cool.compiler.autogen.analysis.*;
import cool.compiler.ref.tables.AbstractTable;

@SuppressWarnings("nls")
public final class TTypeId extends Token
{
    public TTypeId(String text)
    {
        setText(text);
        symbol = AbstractTable.idtable.addString(text);
    }

    public TTypeId(String text, int line, int pos)
    {
        setText(text);
        setLine(line);
        setPos(pos);
        symbol = AbstractTable.idtable.addString(text);
    }

    @Override
    public Object clone()
    {
      return new TTypeId(getText(), getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTTypeId(this);
    }
}
